<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SHARP | Predict Success') }}</title>
      <link rel="icon" href="{{ url('images/favicons.png') }}" type="image/png">

    <!-- CSS  -->
    <!-- Styles -->
    <link href="../public/css/app.css" rel="stylesheet">
	<link href="../public/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOj0_u0DRE2dK8X9YptdCXtxt89UCqfoo&amp;sensor=true"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <!-- Styles * End -->
    <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
    <script src="../public/js/jquery-3.2.1.min.js"></script>
    <script src="../public/js/jquery.timeago.min.js"></script>
    <script src="../public/js/prism.js"></script>
    <script src="../public/js/lunr.min.js"></script>
    <script src="../public/js/search.js"></script>
    <script src="../public/js/materialize.js"></script>
    <script src="../public/js/init.js"></script>
    <script src="../public/js/platform.js" async="" defer="" gapi_processed="true"></script>
</head>
<body>

 <?php use App\Http\Controllers\HospitalController;?>
      <?php $arrv = HospitalController::getHpl(Auth::user()->hospital_id);?>
    
            <!-- Sub Menues ** END -->
      <?php use App\Http\Controllers\UserController;?>
     <?php $arr2 = UserController::getRoleID(Auth::user()->id);?>
<ul id="nav-mobile" class="sidenav sidenav-fixed">
        <li class="logo" style="margin-bottom: 31px;"><a id="logo-container" href="#" class="brand-logo">
      <img src="{{ url('images/sharp_logo.png') }}" alt="logo" class="logo-default" style="width:150px;"></a></li>
       <li class="bold active" ><a href="home" class="waves-effect waves-teal" ><i class="small material-icons">home</i>Dashboard</a></li>

        @if ($arr2 == '1')
          <li>
            <a href="{{ url ('hospitalslist') }}"><i class="small material-icons">table</i>Hospitals</a>
          </li>
       @endif
    
      @if ($arr2 == '2')
        <li>
            <a href="hospitals?i={{Auth::user()->hospital_id }}"><i class="small material-icons">table</i>Hospitals</a>
           </li>
       @endif       
       <li>
	   <a href="{{ url('/logout') }}" onClick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="small material-icons" >account_circle</i>Logout</a>
	   </li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
      
      </ul>
     
    <nav class="top-nav cyan">
      <div class="nav-wrapper">
       <div ><a href="#" data-target="nav-mobile" class="top-nav sidenav-trigger waves-effect waves-light circle hide-on-large-only"><i class="material-icons white">menu</i></a></div>
       <ul id="nav-mobile" class="right hide-on-med-and-down">
         @if ($arr2 == '2')
              <!-- Sub Menues ** END --><h5 style="margin-left:225px;float:left;"> {{ $arrv }} </h5>
         @endif
        <li><b> Welcome  {{ Auth::user()->firstname }} {{ Auth::user()->lastname }} ! &nbsp;&nbsp; </b></li>
        </ul>
      </div>
     </nav>
  @yield('content')


<footer class="page-footer cyan m200">
    
    <div class="footer-copyright" style="width:100%">
		<div style="margin-left:1%;">
         <span class="left-align">Powered by KAT Enterprise LLC</span>
         <span class="right-align" style="right:15px;float:right">Copyright © <a style="font-weight: bold; color:white;" href="https://katenterprise.com/" target="_blank">2018 MyHomeCareBiz.com</a></span>
      </div>
    </div>
</footer>

 
<!--  Scripts-->
<script src="js/app.js"></script>
<script type="text/javascript">

function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
{
return false;
}	
return true;
} 
</script>
<!--  Scripts-- * End JS -->
@yield('scripts')


</body>
</html>
