<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SHARP | Predict Success') }}</title>
      <link rel="icon" href="{{ url('images/favicons.png') }}" type="image/png">

    <!-- CSS  -->
    <!-- Styles -->
    <link href="../public/css/app.css" rel="stylesheet">
	<link href="../public/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOj0_u0DRE2dK8X9YptdCXtxt89UCqfoo&amp;sensor=true"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles * End -->
	 <!-- Styles * End -->
    <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://materializecss.com/docs/js/jquery.timeago.min.js"></script>
    <script src="https://materializecss.com/docs/js/prism.js"></script>
    <script src="https://materializecss.com/docs/js/lunr.min.js"></script>
    <script src="https://materializecss.com/docs/js/search.js"></script>
    <script src="https://materializecss.com/bin/materialize.js"></script>
	<script src="https://materializecss.com/docs/js/init.js"></script>
    <script src="https://apis.google.com/js/platform.js" async="" defer="" gapi_processed="true"></script>
</head>
<body>
<div class="navbar-fixed">
    <nav class="top-nav cyan" role="navigation">
        <div class="nav-wrapper">            
         <?php use App\Http\Controllers\HospitalController;?>
		<?php $arrv = HospitalController::getHpl(Auth::user()->hospital_id);?>
		
            <!-- Sub Menues ** END -->
		<?php use App\Http\Controllers\UserController;?>
		<?php $arr2 = UserController::getRoleID(Auth::user()->id);?>
		@if ($arr2 == '2')
            <!-- Sub Menues ** END --><h5 style="margin-left:225px;float:left;"> {{ $arrv }} </h5>
	@endif
	
            <ul class="right hide-on-med-and-down" style="margin-right: 10px;margin-top:20px;">
             
                <li class="account avatar dropdown-button rht" data-activates="account-dropdown" data-beloworigin="true">
					 <b> Welcome  {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</b>
                </li>
            </ul>
			
			<a href="#" data-target="nav-mobile" class="top-nav sidenav-trigger full hide-on-large-only"><i class="material-icons">menu</i></a>
			
           <!-- <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>-->
        </div>
    </nav>
</div>
<ul id="nav-mobile" class="sidenav sidenav-fixed" >
        <li class="logo"><a id="logo-container" href="/" class="brand-logo">
            <object id="front-page-logo" type="image/svg+xml" data="res/materialize.svg">Your browser does not support SVG</object></a></li>
        <li class="version"><a href="#" data-target="version-dropdown" class="dropdown-trigger">
            1.0.0
            <svg class="caret" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></a><ul id="version-dropdown" class="dropdown-content" tabindex="0">
            <li tabindex="0"><a>1.0.0</a></li>
            <li tabindex="0"><a href="http://archives.materializecss.com/0.100.2/">0.100.2</a></li>
          </ul>
         
        </li>
        <li class="search">
          <div class="search-wrapper">
            <input id="search" placeholder="Search"><i class="material-icons">search</i>
            <div class="search-results"></div>
          </div>
        </li>
        <li class="bold"><a href="about.html" class="waves-effect waves-teal">About</a></li>
        <li class="bold"><a href="getting-started.html" class="waves-effect waves-teal">Getting Started</a></li>
        <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-teal" tabindex="0">CSS</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="color.html">Color</a></li>
                  <li><a href="grid.html">Grid</a></li>
                  <li><a href="helpers.html">Helpers</a></li>
                  <li><a href="media-css.html">Media</a></li>
                  <li><a href="pulse.html">Pulse</a></li>
                  <li><a href="sass.html">Sass</a></li>
                  <li><a href="shadow.html">Shadow</a></li>
                  <li><a href="table.html">Table</a></li>
                  <li><a href="css-transitions.html">Transitions</a></li>
                  <li><a href="typography.html">Typography</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-teal" tabindex="0">Components</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="badges.html">Badges</a></li>
                  <li><a href="buttons.html">Buttons</a></li>
                  <li><a href="breadcrumbs.html">Breadcrumbs</a></li>
                  <li><a href="cards.html">Cards</a></li>
                  <li><a href="collections.html">Collections</a></li>
                  <li><a href="floating-action-button.html">Floating Action Button</a></li>
                  <li><a href="footer.html">Footer</a></li>
                  <li><a href="icons.html">Icons</a></li>
                  <li><a href="navbar.html">Navbar</a></li>
                  <li><a href="pagination.html">Pagination</a></li>
                  <li><a href="preloader.html">Preloader</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-teal" tabindex="0">JavaScript</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="auto-init.html">Auto Init</a></li>
                  <li><a href="carousel.html">Carousel</a></li>
                  <li><a href="collapsible.html">Collapsible</a></li>
                  <li><a href="dropdown.html">Dropdown</a></li>
                  <li><a href="feature-discovery.html">FeatureDiscovery</a></li>
                  <li><a href="media.html">Media</a></li>
                  <li><a href="modals.html">Modals</a></li>
                  <li><a href="parallax.html">Parallax</a></li>
                  <li><a href="pushpin.html">Pushpin</a></li>
                  <li><a href="scrollspy.html">Scrollspy</a></li>
                  <li><a href="sidenav.html">Sidenav</a></li>
                  <li><a href="tabs.html">Tabs</a></li>
                  <li><a href="toasts.html">Toasts</a></li>
                  <li><a href="tooltips.html">Tooltips</a></li>
                  <li><a href="waves.html">Waves</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-teal" tabindex="0">Forms</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="autocomplete.html">Autocomplete</a></li>
                  <li><a href="checkboxes.html">Checkboxes</a></li>
                  <li><a href="chips.html">Chips</a></li>
                  <li><a href="pickers.html">Pickers</a></li>
                  <li><a href="radio-buttons.html">Radio Buttons</a></li>
                  <li><a href="range.html">Range</a></li>
                  <li><a href="select.html">Select</a></li>
                  <li><a href="switches.html">Switches</a></li>
                  <li><a href="text-inputs.html">Text Inputs</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li class="bold"><a href="mobile.html" class="waves-effect waves-teal">Mobile</a></li>
        <li class="bold"><a href="showcase.html" class="waves-effect waves-teal">Showcase</a></li>
        <li class="bold"><a href="themes.html" class="waves-effect waves-teal">Themes<span class="new badge"></span></a></li>
      </ul>
  </div>

			@yield('content')
<footer class="page-footer cyan m200">
    
    <div class="footer-copyright" style="width:100%">
		<div style="margin-left:1%;">
         <span class="left-align">Powered by KAT Enterprise LLC</span>
         <span class="right-align" style="right:15px;float:right">Copyright © <a style="font-weight: bold; color:white;" href="https://katenterprise.com/" target="_blank">2018 MyHomeCareBiz.com</a></span>
      </div>
    </div>
</footer>

 
<!--  Scripts-->

<script type="text/javascript">
function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
{
return false;
}	
return true;
}
</script>
<!--  Scripts-- * End JS -->
@yield('scripts')


</body>
</html>
