<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SHARP | Predict Success') }}</title>
      <link rel="icon" href="{{ url('images/favicons.png') }}" type="image/png">

    <!-- CSS  -->
    <!-- Styles -->
    <link href="../public/css/app.css" rel="stylesheet">
	<link href="../public/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOj0_u0DRE2dK8X9YptdCXtxt89UCqfoo&amp;sensor=true"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles * End -->
</head>
<body>
<div class="navbar-fixed">
    <nav class="top-nav cyan" role="navigation">
        <div class="nav-wrapper">            
         <?php use App\Http\Controllers\HospitalController;?>
		<?php $arrv = HospitalController::getHpl(Auth::user()->hospital_id);?>
		
            <!-- Sub Menues ** END -->
		<?php use App\Http\Controllers\UserController;?>
		<?php $arr2 = UserController::getRoleID(Auth::user()->id);?>
		@if ($arr2 == '2')
            <!-- Sub Menues ** END --><h5 style="margin-left:225px;float:left;"> {{ $arrv }} </h5>
	@endif
	
            <ul class="right hide-on-med-and-down" style="margin-right: 10px;margin-top:20px;">
             
                <li class="account avatar dropdown-button rht" data-activates="account-dropdown" data-beloworigin="true">
					 <b> Welcome  {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</b>
                </li>
            </ul>
			
			
			
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>
<ul id="nav-mobile" class="side-nav fixed" style="padding-top:10px;">
<li class="logo"><a id="logo-container" href="#" class="brand-logo"><img src="http://192.168.50.24:90/sharp_admin/public/images/sharp_logo.png" alt="logo" class="logo-default"></a></li>


      <li><a href="home"><i class="material-icons">home</i>Dashboard</a></li>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
	    @if ($arr2 == '1')
          <li>
            <a href="{{ url ('hospitalslist') }}"><i class="material-icons">table</i>Hospitals</a>
          </li>
        </ul>
      </li>
	  	@endif
		
		  @if ($arr2 == '2')
          <li>
            
          </li>
        </ul>
      </li>
	  	@endif
	   @if ($arr2 == '1')
	    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
        </ul>
      </li>
	   @endif
	   <li><a href="{{ url('/logout') }}" onClick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">account_circle</i>Logout</a></li>
         <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
           		{{ csrf_field() }}
          </form>
    </ul>
  </div>

			@yield('content')
<footer class="page-footer cyan m200">
    
    <div class="footer-copyright" style="width:100%">
		<div style="margin-left:1%;">
         <span class="left-align">Powered by KAT Enterprise LLC</span>
         <span class="right-align" style="right:15px;float:right">Copyright © <a style="font-weight: bold; color:white;" href="https://katenterprise.com/" target="_blank">2018 MyHomeCareBiz.com</a></span>
      </div>
    </div>
</footer>

 
<!--  Scripts-->
<script src="js/app.js"></script>
<script type="text/javascript">
function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
{
return false;
}	
return true;
}
</script>
<!--  Scripts-- * End JS -->
@yield('scripts')


</body>
</html>
