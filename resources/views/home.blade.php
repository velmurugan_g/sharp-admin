@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	<div class="box-body">
                    <div class="row">
                        <div>
                                <!--    <div class="col s3">
                                        <div class="dashboard-stat bg-aqua">
                                            <div class="visual">
                                                <i class="fa fa-dashboard"></i>
                                            </div>
                                            <div class="details">
                                                <div class="number sitemap-box">
                                                    <a href="dashboard">Dashboard</a>
                                                </div>
                                                <div class="desc custome-sitemap-link">
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
									 <?php use App\Http\Controllers\HospitalController;?>
									 <?php $hplcnt = HospitalController::Hplcount();?>
									<?php use App\Http\Controllers\UserController;?>
		<?php $arr2 = UserController::getRoleID(Auth::user()->id); $getcntval = UserController::getTotalUser(); $getcntval2 = UserController::getTotalUser2(Auth::user()->hospital_id);?>

   
    <?php $Physician_count  = DB::select('select distinct(count(*)) as Physician_count from admin_user_entity JOIN admin_user_roles_entity on admin_user_roles_entity.user_id = admin_user_entity.id  where role_id = 3  AND  deleted <> 1'); 
     $discharge_count  = DB::select('select distinct(count(*)) as discharge_count from admin_user_entity JOIN admin_user_roles_entity on admin_user_roles_entity.user_id = admin_user_entity.id  where role_id = 8 AND  deleted <> 1');  
      $caregivers_count  = DB::select('select distinct(count(*)) as caregivers_count from admin_user_entity JOIN admin_user_roles_entity on admin_user_roles_entity.user_id = admin_user_entity.id  where role_id = 6  AND  deleted <> 1');  
       $patient_count  = DB::select('select distinct(count(*)) as patient_count from admin_user_entity JOIN admin_user_roles_entity on admin_user_roles_entity.user_id = admin_user_entity.id  where role_id = 5 AND  deleted <> 1');  
      $vendor_count  = DB::select('select distinct(count(*)) as vendor_count from admin_user_entity JOIN admin_user_roles_entity on admin_user_roles_entity.user_id = admin_user_entity.id  where role_id = 9 AND  deleted <> 1');  ?>
        
	    @if ($arr2 == '1')
		
		 <div class="col s12 m6 l6">
        <div style="padding: 35px;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>Hospital Management</b>
            </div>
          </div>

          <div class="row">
            <a href="hospitalslist">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">store</i>
                <span class="cyan-text text-lighten-1"><h6>{{$hplcnt}} Hospitals</h6></span>
              </div>
            </a>
            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="userslist">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">people</i>
                <span class="cyan-text text-lighten-1"><h6>{{$getcntval}} Users</h6></span>
              </div>
            </a>
          </div>
        </div>
      </div>

      <div class="col s12 m6 l6"> 
        <div id="chart-container"></div> 
 </div>
	 <!-- 
	  <div class="col s6">
        <div style="padding: 35px;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>API Management</b>
            </div>
          </div>
          <div class="row">
            <a href="#!">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">store</i>
                <span class="cyan-text text-lighten-1"><h6>API List</h6></span>
              </div>
            </a>

            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="#!">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">assignment</i>
                <span class="cyan-text text-lighten-1"><h6>Settings</h6></span>
              </div>
            </a>
          </div>
        </div>
      </div>-->
@endif
@if ($arr2 == '2')
	 <div class="col s6">
        <div style="padding: 35px;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>Hospital Management</b>
            </div>
          </div>

          <div class="row">
            <a href="#">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">store</i>
                <span class="cyan-text text-lighten-1"><h6>Hospitals</h6></span>
              </div>
            </a>
            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="#">
              <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                <i class="cyan-text text-lighten-1 large material-icons">people</i>
                <span class="cyan-text text-lighten-1"><h6>Users</h6></span>
              </div>
            </a>
          </div>
        </div>
      </div>
@endif
         </div>
            </div>
              </div>
            

        </div>
        <div class="section">		 
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
    <script type="text/javascript" src="js/fusioncharts.js"></script>
<script type="text/javascript" src="js/themes/fusioncharts.theme.fusion.js"></script>
<script type="text/javascript">
   //var caregivers_count = {{json_encode($hplcnt)}};
  var caregivers_count =  <?php echo json_encode($caregivers_count) ?>;
  var Physician_count =  <?php echo json_encode($Physician_count) ?>;
  var discharge_count =  <?php echo json_encode($discharge_count) ?>;
  var patient_count =  <?php echo json_encode($patient_count) ?>;
  var vendor_count =  <?php echo json_encode($vendor_count) ?>;
   FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'pie3d',
    renderAt: 'chart-container',
    width: '600',
    height: '350',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Users Categories",
            //"subCaption": "Last year",
            //"numberPrefix": "$",
            "showPercentInTooltip": "1",
           // "decimals": "1",
            "useDataPlotColorForLabels": "1",
            //Theme
            "theme": "fusion"
        },
        "data": [
            {  "label": "Physician","value": Physician_count[0].Physician_count },
            {  "label": "Discharge Planner","value": discharge_count[0].discharge_count },
            { "label": "Caregivers", "value": caregivers_count[0].caregivercount }, 
            {"label": "Patients", "value": patient_count[0].patient_count},
            { "label": "Vendors","value":  vendor_count[0].vendor_count}
              ]
    }
}
);
    fusioncharts.render();
    });
</script>
	
@stop
