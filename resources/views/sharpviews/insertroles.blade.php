@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
<form action="{{ url('/createrole') }}" method="post" class="form-horizontal" role="form">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					  <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>Role Details</b></h5>
					 </div>
					<div class="row">
					<div class="input-field col s6">
					<input name="rolename" id="rolename" autofocus required type="text" class="validate" value="">
					<label for="rolename" class="active">Role Name</label>
					</div>
					<div class="input-field col s6">
					<textarea name="description" id="description" required type="text" class="validate" value=""  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="description" class="active">Role Description</label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
	  				<select class="browser-default" name="accesslevel">
      <option value="" disabled selected>Choose your Access Level</option>
      <option value="1">Access Level I</option>
      <option value="2">Access Level II</option>
    </select>
					</div>
					</div>
                     	<div class="row">
					<div class="input-field col s3 right">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					 <input type="submit" value="Submit" class="sa col s6 btn btn-small waves-effect cyan waves-input-wrapper">
					</div>
					</div>
                    </div>
                </div>
            </div>
	</form>
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="../public/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../public/js/init.js"></script>
		<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
@stop
