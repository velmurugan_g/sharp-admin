@extends('layouts.app')

@section('content')
	<?php use App\Http\Controllers\HospitalController;?>
		<?php use App\Http\Controllers\ApiController;?>
    <div class="container m220">
        <div class="section">
<form action="{{ url('/createapi') }}" method="post" class="form-horizontal" role="form">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>API Management</b></h5>
					 </div>
					 
		<div class="row">			
		<div class="input-field col s6">
		<?php $providerlist = ApiController::getproviderList();?>
		 <select class="browser-default"id="providername" name="providername" >
	     	@foreach($providerlist as $key => $data)
		<option value="{{$data->id}}">{{$data->name}}</option>
	  		
			@endforeach
						</select>
						
					<label for="providername" class="active">Provider Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					
		          		<?php $hpllist = HospitalController::getHplList();?>
						
							<select class="browser-default" name="hospital_id">
								 <!--<option value="" disabled="" selected="">Choose Hospital Name</option>-->
								 @foreach($hpllist as $key => $data)
								<?php if ($_GET["i"]==$data->id) { ?>
								<option value="{{$data->id}}">{{$data->name}}</option>
								<?php } ?>
								@endforeach
							</select>
					<label for="email" class="active">Hospital<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="accesstoken" id="accesstoken" required type="text" class="validate" value="" >
					<label for="accesstoken" class="active">Access Token<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="accessurl" id="accessurl" type="text" class="validate" value="" >
					<label for="accessurl" class="active">Access URL</label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="clientid" id="clientid" required type="text" class="validate" value="" >
					<label for="clientid" class="active">Client ID<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="secretkey" id="secretkey" required type="text" class="validate" value="" >
					<label for="secretkey" class="active">Secret Key<span class="red-text">*</span></label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
		<textarea name="otherservices" id="otherservices" type="text" class="validate" value=""  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="zip" class="active">Other Services</label>
					</div>
					
					</div>
		             <?php $acllist = ApiController::getAccessMethods();?>			
					<h6>Accessible Methods</h6>
				<div class="row">
				@foreach($acllist as $key => $data)
					<div class="col s2">
						<label><input id="acc_{{$data->id}}" name="accessmethods" type="checkbox" value="{{$data->id}}" class="filled-in"><span class="green-text"><b>{{$data->method_name}}</b></span> </label>
					</div>
				@endforeach	
				</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="user_id" value="<?php echo Auth::user()->id ?>">
					 <input type="submit" value="Submit" class="btn cyan">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>

<style>
body {
	font-family : helvetica, arial;
}
button {
	padding: 5px;
	font-size: 120%;
}
.search_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 9999;
	right: 100px;
	top: 49px;
}
.clear_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 9999;
	right:164px;
	top: 49px;
}
/*textarea {
	float: left;
	clear: none;
	width: 200px;
	height: 360px;
	margin-left: 10px;
}*/
#map-canvas {
	width:700px;
	height:400px;
	float: left;
	clear: none;
}
</style>

@stop
