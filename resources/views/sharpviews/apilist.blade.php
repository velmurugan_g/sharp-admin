@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	<?php use App\Http\Controllers\ApiController;?>
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">List of API</span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" <?php /*?>href="{{ url ('insertapi') }}"<?php */?>  href="insertapi?i=<?php echo $_GET["i"]; ?>"><i class="material-icons">add</i> Add API</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable3">
                            <thead>
                            <tr>
                                <th>Provider Name</th>
                                <th>Access Token</th>
                                <th>Client ID</th>
                                <th>Secret Key</th>
								<th>Others Services</th>                                
                            </tr>
                            </thead>
                            <tbody>
							<?php /*?> <?php $getapidetails = ApiController::getAPI(Auth::user()->hospital_id);?><?php */?>
							 	<?php $getapidetails = ApiController::getAPI($_GET["i"]);?>
                        @foreach($getapidetails as $key => $data)
    <tr>    
      <td><a href="editapi?i={{$data->id}}" style="color:#0099FF">{{$data->providername}}</a></td>
      <td>{{$data->access_token}}</td>
      <td>{{$data->client_id}}</td>
      <td>{{$data->secret_key}}</td>
      <td>{{$data->others}}</td>                 
    </tr>
@endforeach
</tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
@stop
