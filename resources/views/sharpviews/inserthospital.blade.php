@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
<form action="{{ url('/createhospital') }}" method="post" class="form-horizontal" role="form" name="add_hospital">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					  <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>Hospital Details</b></h5>
					 </div>
					 
					<div class="row">
					<div class="input-field col s6">
					<input name="name" id="name" autofocus required type="text" class="validate" value="">
					<label for="name" class="active">Hospital Name <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="city" id="city"  required type="text" class="validate" value="">
					<label for="city" class="active">City <span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="owner" id="owner" required type="text" class="validate" value="" >
					<label for="owner" class="active">Owner of Hospital <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="state" id="state" required type="text" class="validate" value="" >
					<label for="state" class="active">State <span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="contactperson" id="contactperson" required type="text" class="validate" value="" >
					<label for="contact_person" class="active">Contact Person <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="zip" id="zip" required type="text" class="validate" value="" onKeyPress="return isNumberKey(event)" maxlength="5">
					<label for="zip" class="active">Zip Code <span class="red-text">*</span></label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="email" id="email" required type="text" class="validate" value="" >
					<label for="email" class="active">Contact Email <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="weburl" id="web_url" type="text" class="validate" value="" >
					<label for="weburl" class="active">Web URL</label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
					<input name="contactnumber" id="contact_number" required type="text" class="validate" value=""  maxlength="11" onKeyPress="return isNumberKey(event)">
					<label for="contactnumber" class="active">Contact Number <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<textarea name="streetaddress" id="street_address" required type="text" class="validate" value=""  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="streetaddress" class="active">Street Address <span class="red-text">*</span></label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
					<input name="npi" id="npi" type="text" class="validate" value="" >
					<label for="npi" class="active">NPI </label>
					</div>
					<div class="input-field col s6">
					<input name="medicareid" id="medicareid" type="text" class="validate" value="" >
					<label for="medicareid" class="active">Medicare ID</label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="medicareprovider" id="medicareprovider" type="text" class="validate" value="" >
					<label for="medicareprovider" class="active">Medicare Provider</label>
					</div>
					<div class="input-field col s6">
					<textarea name="description" id="description" type="text" class="validate" value=""  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="description" class="active">Description</label>
					</div>
					</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="button" value="Submit" class="btn cyan"  onclick="return validation();">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="../public/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../public/js/init.js"></script>
			<script type="text/javascript">
function goBack() {
window.history.back();
}

function validation(){
if((document.getElementById("name").value).trim() == "")
{
Materialize.toast('Please enter the hospital name ', 4000, 'red');
}
else if((document.getElementById("city").value).trim() == "" ){
Materialize.toast('Please enter the city', 4000, 'red');
}
else if((document.getElementById("owner").value).trim() == "" ){
Materialize.toast('Please enter the owner of hospital ', 4000, 'red');
}
else if((document.getElementById("state").value).trim() == ""){
Materialize.toast('Please enter the state', 4000, 'red');
}
else if((document.getElementById("contactperson").value).trim() == ""){
Materialize.toast('Please enter the contact person', 4000, 'red');
}
else if((document.getElementById("zip").value).trim() == ""){
Materialize.toast('Please enter the zip code', 4000, 'red');
}
else if((document.getElementById("email").value).trim() == ""){
Materialize.toast('Please enter the email', 4000, 'red');
}
else if((document.getElementById("contact_number").value).trim() == ""){
Materialize.toast('Please enter the contact number', 4000, 'red');
}
else if((document.getElementById("street_address").value).trim() == ""){
Materialize.toast('Please enter the street address', 4000, 'red');
}


/*else if($('#service_id').find('input[name=service_id]:checked').length == "")
    {
      Materialize.toast('Please select atleast one service details', 4000, 'red');
    }
	
	else if($('#defaultvendor').find('input[name=defaultvendor]:checked').length == "")
    {
       Materialize.toast('Please select default vendor', 4000, 'red');
    }*/

else{

hospital_checkclick();
}

} 



function hospital_checkclick()
{
//alert("check");
//return false;
        $.ajax({
                type:'get',
                url:'getemail',   
                data: {
                 email: document.getElementById("email").value,               
                 contactnumber: document.getElementById("contact_number").value
                 },
                 success: function (data){
                   //alert(data);
                   if (data == 'email')
                   {Materialize.toast('Email already exists,Kindly change different email', 4000, 'red');
                   return false;}
                   else if (data == 'phonenumber')
                   {Materialize.toast('Phone number already exists,Kindly change different phonenumber', 4000, 'red');
                   return false;}
                   else if (data == 'sucess'){
                     add_hospital.submit();
                   }
                },
                error: function (xhr, textStatus, errorThrown){
                alert(errorThrown);
                }
                });   
      // return false;
}


</script>
@stop
