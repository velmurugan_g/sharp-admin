@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
		<?php use App\Http\Controllers\VendorController;?>
		<?php $getUserIDs = VendorController::getUserID($_GET["i"]);?>
		@foreach($getUserIDs as $key => $data)
<form action="{{ url('/updatevendor') }}" method="post" class="form-horizontal" role="form"  name="edit_vendor">
	<input type="hidden" name="user_id" id="user_id" value="{{$data->user_id}}">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>Edit Vendor Details</b></h5>
					 </div>
					 
					<div class="row">
					<div class="input-field col s6">
					<input name="vendorname" id="vendorname" autofocus  type="text" class="validate" value="{{$data->name}}">
					<label for="vendorname" class="active">Vendor Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="email" id="email"   type="text" class="validate" value="{{$data->email}}">
					<label for="email" class="active">Email id<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="username" id="username" type="text"  class="validate" value="{{$data->username}}" >
					<label for="username" class="active">Username<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="password" id="password" type="password"  class="validate" value="{{$data->password}}" >
					<label for="password" class="active">Password<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="city" id="city" type="text" class="validate"  value="{{$data->city}}" >
					<label for="city" class="active">City<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="state" id="state" type="text" class="validate"  value="{{$data->state}}" >
					<label for="state" class="active">State<span class="red-text">*</span></label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="zip" id="zip" type="text" class="validate" value="{{$data->zip}}"   maxlength="5"onKeyPress="return isNumberKey(event)">
					<label for="zip" class="active">Zip<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="phonenumber" id="phonenumber" type="text" maxlength="10"  class="validate" value="{{$data->phonenumber}}" onKeyPress="return 				                     isNumberKey(event)">
					<label for="phonenumber" class="active">Phone Number<span class="red-text">*</span></label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
						<textarea name="streetaddress1" id="streetaddress1" type="text" class="validate" value="{{$data->address1}}"  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="streetaddress1" class="active">Street Address 1</label>
					</div>
					<div class="input-field col s6">
					<textarea name="streetaddress2" id="streetaddress2" type="text" class="validate" value="{{$data->address2}}"  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="streetaddress2" class="active">Street Address 2</label>
					</div>
					</div>
					
					<h6>Service Details<span class="red-text">*</span></h6>
			<div class="row">
			<div class="input-field col s12">
		                                <?php $adminlist = VendorController::getAdminVendorServices(); $vendlist = VendorController::getTasklookup();?>
										
										<div class="row">
									@foreach($adminlist as $key => $datasa)
									<p class="input-field col s6 m6 l3" style="padding-bottom:7px;">
									  <label>
										<input id="service_{{$datasa->id}}" name="vendservices[]" type="checkbox" <?php $dadlist = VendorController::getserviceRecords($data->id,$data->hospital_id,$datasa->id); if($dadlist==$datasa->id) echo "checked='checked'"?> value="{{$datasa->id}}" class="filled-in" /><span style="line-height:16px;">{{$datasa->services}}</span>
									  </label>
									</p>
									@endforeach
																
								</div>
								
								
			</div><br />
			<h6>Please check the below one for Default Vendor ?</h6>
			<div class="row">
	<div class="input-field col s6">
								<p>
									  <label>
										<input id="defaultvendor" name="defaultvendor" type="checkbox" <?php if($data->isdefault==1) echo "checked='checked'"; ?> class="filled-in"><span style="line-height:16px;">Default Vendor</span> 
									  </label>
									</p>
								</div>
								
								<?php /*?><!--<?php $getvendortypelists = VendorController::getvendor_type_All(); $getvendor_type = VendorController::getvendortype($data->descrip); ?>
						<label for="default_vendor" class="active"></label><br />
							<select class="browser-default" required name="vendor_type">
							 <option value="" disabled selected><?php  echo ucfirst($getvendor_type); ?></option>
								@foreach($getvendortypelists as $key => $datas)
								<?php if($getvendor_type!=$datas->description) ?>
								<option value= "{{$datas->descrip}}">{{$datas->description}}</option>
								@endforeach
								</select>--><?php */?>
								
								<div class="input-field col s6" style="margin-top:-4%">
								<h6>Default Vendor<span class="red-text">*</span></h6>
								  <label for="default_vendor" class="active"></label><br/>
                        <select class="browser-default"  id="vendor_type" name="vendor_type" value="{{$data->vendor_type}} >
                        <option value="" disabled selected>Select vendor type</option>
                    @if ($data->vendor_type == "Home Health Agency")
                   <option value="Home Health Agency" selected>Home Health Agency</option>
                   @else  
                   <option value="Home Health Agency">Home Health Agency</option>                    
                  @endif     
                   @if ($data->vendor_type == "Medical Supplier")
                     <option value="Medical Supplier" selected>Medical Supplier</option>  
                      @else
                     <option value="Medical Supplier">Medical Supplier</option>
                     @endif
                          @if ($data->vendor_type == "HCM")
                     <option value="Social Services Agency" selected>Social Services Agency</option>  
                      @else
                     <option value="Social Services Agency">Social Services Agency</option>
                     @endif
                     </select> 
						</div>
								
			</div>
				<div class="row">
				<div class="input-field col s6">
				<label for="managepatients" class="active" style="font-size:1.2rem">How many Patients will manage in a Single day ?<span class="red-text">*</span></label>
					<input name="managepatients" id="managepatients" type="text" class="validate"  value="{{$data->patient_manage}}" onKeyPress="return isNumberKey(event)">
					
					</div>
					<div class="input-field col s6">
					<label for="phonenumber" class="active" style="font-size:1.2rem">Quality Level<span class="red-text">*</span></label>
					<input name="qualitylevel" id="qualitylevel" type="text"  class="validate" value="{{$data->quality_level}}" onKeyPress="return isNumberKey(event)">
					
					</div>
			</div>	
					<div class="row">
						<div class="input-field col s8">
						<h6>Map Plotting</h6>
						<button id="search" class="search_button">Search</button>
						<button id="clear_button" class="clear_button" onclick="clearall();">Clear</button>
<hr>
<div id="map-canvas"></div>

												</div>
												<div class="input-field col s4">
												<h6>Coverage Area<span class="red-text">*</span></h6>
<textarea id="zipcodes" required name="zipcodes" style="height:360px;">{{$data->coveragearea}}</textarea>
												</div>
							
						</div>
					<h6>Days Available</h6>
					<div class="row blue-text">
									      <div class="col l1"></div><div class="col l1"></div>
										  
										 
										   <!--<div class="col s6 m4 l1">
											 <label><input id="alls" name="alls" type="checkbox" value="all" class="filled-in" onclick="toggle(this);"><span class="green-text"><b>ALL</b></span> </label>
											</div>-->
											
										    <div class="col s6 m4 l1">
											 <label><input id="Sunday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Sunday"); if($adlist =="Sunday") echo "checked='checked'"?> value="Sunday" class="filled-in"><span class="green-text"><b>Sun</b></span> </label>
											 </div>
											 
											 <div class="col s6 m4 l1">
											 <label><input id="Monday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Monday"); if($adlist =="Monday") echo "checked='checked'"?> value="Monday" class="filled-in"><span class="green-text"><b>Mon</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Tuesday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Tuesday"); if($adlist =="Tuesday") echo "checked='checked'"?> value="Tuesday" class="filled-in"><span class="green-text"><b>Tue</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Wednesday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Wednesday"); if($adlist =="Wednesday") echo "checked='checked'"?> value="Wednesday" class="filled-in"><span class="green-text"><b>Wed</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Thursday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Thursday"); if($adlist =="Thursday") echo "checked='checked'"?> value="Thursday" class="filled-in"><span class="green-text"><b>Thu</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Friday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Friday"); if($adlist =="Friday") echo "checked='checked'"?> value="Friday" class="filled-in"><span class="green-text"><b>Fri</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Saturday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Saturday"); if($adlist =="Saturday") echo "checked='checked'"?> value="Saturday" class="filled-in"><span class="green-text"><b>Sat</b></span> </label>
											 </div>
											 </div>
										<h6>Tasks Available</h6>
										<?php $vendlist = VendorController::getTasklookup();?>
										<div class="row">
									@foreach($vendlist as $key => $datas)
									<p class="input-field col s6 m6 l3" style="padding-bottom:7px;">
									  <label>
										<input id="{{$datas->Id}}" name="availabletime[]" type="checkbox" <?php $dadlist = VendorController::gettimeRecords($data->id,$datas->Id); if($dadlist==$datas->Id) echo "checked='checked'"?> value="{{$datas->Id}}" class="filled-in" /><span style="line-height:16px;">{{$datas->Task}}</span>
									  </label>
									</p>
									@endforeach				
									</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="vendor_id" value="<?php echo $_GET["i"]; ?>">
			   <input type="hidden" name="hospital_id" value="{{$data->hospital_id}}">
					 <input type="button" value="Submit" class="btn cyan" onclick="editvendor_mail_check();">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>
			@endforeach
        </div>
        <br><br>
    </div>
	</div>
@endsection

@section('scripts')
<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
<style>
body {
	font-family : helvetica, arial;
}
button {
	padding: 5px;
	font-size: 120%;
}
.search_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 1;
	right: 100px;
	top: 49px;
}
.clear_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 1;
	right:164px;
	top: 49px;
}
/*textarea {
	float: left;
	clear: none;
	width: 200px;
	height: 360px;
	margin-left: 10px;
}*/
#map-canvas {
	width:700px;
	height:400px;
	float: left;
	clear: none;
}
</style>
<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
<style>
body {
	font-family : helvetica, arial;
}
button {
	padding: 5px;
	font-size: 120%;
}
.search_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 1;
	right: 100px;
	top: 49px;
}
.clear_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 1;
	right:164px;
	top: 49px;
}
/*textarea {
	float: left;
	clear: none;
	width: 200px;
	height: 360px;
	margin-left: 10px;
}*/
#map-canvas {
	width:700px;
	height:400px;
	float: left;
	clear: none;
}
</style>
<script type="text/javascript">

var usaCenter = new google.maps.LatLng(39.8106460, -98.5569760);
var poly, map;
var markers = [];
var path = new google.maps.MVCArray;

function editvendor_mail_check()
{
if((document.getElementById("vendorname").value).trim() == "")
{
Materialize.toast('Please enter the vendor name', 4000, 'red');
}
else if((document.getElementById("email").value).trim() == "" ){
Materialize.toast('Please enter the email', 4000, 'red');
}
else if((document.getElementById("username").value).trim() == ""){
Materialize.toast('Please enter the username', 4000, 'red');
}
else if((document.getElementById("password").value).trim() == ""){
Materialize.toast('Please enter the password', 4000, 'red');
}
else if((document.getElementById("city").value).trim() == ""){
Materialize.toast('Please enter the city', 4000, 'red');
}
else if((document.getElementById("state").value).trim() == ""){
Materialize.toast('Please enter the state', 4000, 'red');
}
else if((document.getElementById("zip").value).trim() == ""){
Materialize.toast('Please enter the zip code', 4000, 'red');
}
else if((document.getElementById("phonenumber").value).trim() == ""){
Materialize.toast('Please enter the phonenumber', 4000, 'red');
}
else if((document.getElementById("managepatients").value).trim() == ""){
Materialize.toast('Please enter the How many patients will manage in a Single day ?', 4000, 'red');
}
else if((document.getElementById("qualitylevel").value).trim() == ""){
Materialize.toast('Please enter the quality level', 4000, 'red');
}
else if((document.getElementById("zipcodes").value).trim() == ""){
Materialize.toast('Please select Coverage Area', 4000, 'red');
}
// else if((document.getElementById("vendor_type").selected).trim() == ""){
// Materialize.toast('Please select Coverage Area', 4000, 'red');
// }


/*else if($('#service_id').find('input[name=service_id]:checked').length == "")
    {
      Materialize.toast('Please select atleast one service details', 4000, 'red');
    }
	
	else if($('#defaultvendor').find('input[name=defaultvendor]:checked').length == "")
    {
       Materialize.toast('Please select default vendor', 4000, 'red');
    }*/

else{
edit_vendor_mail_check();
}

} 


function edit_vendor_mail_check()
{
//alert("check");
//return false;
        $.ajax({
                type:'get',
                url:'checkemail',   
                data: {
                 email: document.getElementById("email").value,  
                 user_id: document.getElementById("user_id").value,  
				 username: document.getElementById("username").value,				 
                 contactnumber: document.getElementById("phonenumber").value
                 },
                 success: function (data){
                   //alert(data);
                   if (data == 'email')
                   {Materialize.toast('Email already exists,Kindly change different email', 4000, 'red');
                   return false;}
                   else if (data == 'phonenumber')
                   {Materialize.toast('Phone number already exists,Kindly change different phonenumber', 4000, 'red');
                   return false;}
                   else if (data == 'sucess'){
                     edit_vendor.submit();
                   }
                },
                error: function (xhr, textStatus, errorThrown){
                alert(errorThrown);
				return false;
                }
                });   
      // return false;
}


function createMap() {
	map = new google.maps.Map(document.getElementById("map-canvas"), {
		center: usaCenter,
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		zoomControl: true,
		streetViewControl: false,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		}
	});

	poly = new google.maps.Polygon({
		strokeWeight: 1,
		strokeColor: '#ff0000', 
		fillColor: '#ff0000'
	});
	poly.setMap(map);
	poly.setPaths(new google.maps.MVCArray([path]));

	google.maps.event.addListener(map, 'click', addPolygonPoint);
}

function addPolygonPoint(event) {
	path.insertAt(path.length, event.latLng);
	var marker = new google.maps.Marker({
		icon : 'csquare.png',
		position: event.latLng,
		map: map,
		draggable: true
	});
	markers.push(marker);
	google.maps.event.addListener(marker, 'click', function() {
		marker.setMap(null);
		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		markers.splice(i, 1);
		path.removeAt(i);
	});
	google.maps.event.addListener(marker, 'dragend', function() {
		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		path.setAt(i, marker.getPosition());
	});
}

google.maps.event.addDomListener(window, 'load', createMap);
$("#search").click(function() {
	var latLngs = '';
	for (var i=0;i<markers.length;i++) {
		var ll=markers[i].getPosition().toUrlValue(4);
		latLngs+='['+ll.toString()+']';
	}
	var url='../public/zip/zip.php?latlngs='+latLngs;
	$.ajax({
		url: url, 
		success : function(response) {
			$('#zipcodes').text(response);
		}
	});
	return false;
});

function clearall(){
path.clear();
$('#zipcodes').val("");
return false;
}



$('#username').prop('readonly', true);




</script>

@stop
