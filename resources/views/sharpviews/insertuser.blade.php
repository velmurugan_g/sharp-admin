@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
<form action="{{ url('/createuser') }}" method="post" class="form-horizontal" role="form" name="add_user">
            <div class="row">
			
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>User Details</b></h5>
					 </div>
					<div class="row">
					<div class="input-field col s6">
					<input name="first_name" id="first_name" autofocus required type="text" class="validate" value="">
					<label for="first_name" class="active">First Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="last_name" id="last_name"  required type="text" class="validate" value="">
					<label for="last_name" class="active">Last Name<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="middle_name" id="middle_name" type="text"  class="validate" value="" >
					<label for="middle_name" class="active">Middle Name</label>
					</div>
					<div class="input-field col s6">
					<input name="city" id="city" type="text" class="validate" required value="" >
					<label for="state" class="active">City<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="state" id="state" type="text" class="validate" required value="" >
					<label for="state" class="active">State<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="zip_code" id="zip_code" type="text" class="validate" required value="" maxlength="5" onKeyPress="return isNumberKey(event)">
					<label for="zip_code" class="active">Zip Code<span class="red-text">*</span></label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<label for="gender" class="active" style="font-size:1em">Gender</label><br>
						<select class="browser-default">
     					 <option value="" disabled selected>Choose your option</option>
      					<option value="1">Male </option>
      					<option value="2">Female</option>
   							 </select>
					
					</div>
					<div class="input-field col s6">
					<input name="web_url" id="web_url" type="text"  class="validate" value="" >
					<label for="web_url" class="active">Web URL</label>
					</div>
					</div>
					
					<div class="row">
						<div class="input-field col s6">
							<input name="contact_number" id="contact_number"  type="text" maxlength="11" class="validate" value="" onKeyPress="return isNumberKey(event)">
							<label for="contact_number" class="active">Contact Number<span class="red-text">*</span></label>
						</div>
						<div class="input-field col s6">
							<textarea name="street_address" id="street_address"  type="text" class="validate" value=""  style="border-top:none;border-left:none;border-right:none"></textarea>
							<label for="street_address" class="active">Street Address<span class="red-text">*</span></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<input name="username" id="username"  type="text" class="validate" value="" >
							<label for="username" class="active">User Name<span class="red-text">*</span></label>
						</div>
						<div class="input-field col s6">
							<input name="password" id="password" required type="password" class="validate" value="" >
							<label for="street_address" class="active">Password<span class="red-text">*</span></label>
						</div>
					</div>
					<div class="row">
					<div class="col s6">
					<?php use App\Http\Controllers\UserController;?>
		<?php $arr2 = UserController::getRoleID(Auth::user()->id);?>
					<?php if(isset($_GET['i'])) { ?>
					<input type="hidden" name="hospital_id" value="<?php echo $_GET['i'] ?>" />
					<?php } else  { ?>
		@if ($arr2 == '1')
							<div class="input-field col s12">
							<label for="hospital_name" class="active" style="font-size:1em">Hospital Name<span class="red-text">*</span></label><br />
								<select class="browser-default" name="hospital_id">
								<option value="" disabled selected>Choose your option</option>
								@foreach($gethospitallist as $key => $data)
								<option value= "{{$data->id}}">{{$data->name}}</option>
								@endforeach
								</select>
								
							</div>
		@endif
		@if ($arr2 == '2')
		<input type="hidden" name="hospital_id" value="{{Auth::user()->hospital_id}}" />
		@endif
		<?php } ?>
		<?php use App\Http\Controllers\RoleController;?>
		<?php $getrolelistss = RoleController::getRolesAll(); $getrolelistview = RoleController::getRolesAdmin();?>
		
			@if ($arr2 == '1')
							<div class="input-field col s12">
							<label for="hospital_name" class="active" style="font-size:1em">Role Name <span class="red-text">*</span></label><br />
								<select class="browser-default" name="role_id" id="role_name">
								 <option value="" disabled selected>Choose your option</option>
								@foreach($getrolelistss as $key => $data)
								<option value= "{{$data->id}}">{{$data->rolename}}</option>
								@endforeach
								</select>
							</div>
			@endif
			@if ($arr2 == '2')
							<div class="input-field col s6">
							<label for="hospital_name" class="active">Role Name<span class="red-text">*</span></label><br />
								<select class="browser-default" name="role_id">
								 <option value="" disabled selected>Choose your option</option>
								@foreach($getrolelistview as $key => $data)
								<option value= "{{$data->id}}">{{$data->rolename}}</option>
								@endforeach
								</select>
							</div>
			@endif
					
					
					</div>
						<div class="input-field col s6">
							<input name="email" id="email" required type="text" class="validate" value="" >
							<label for="street_address" class="active">E-mail<span class="red-text">*</span></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6" id="default_caregiver">
						<label for="default_caregiver" class="active" style="font-size:1em">Default vendor<span class="red-text">*</span></label><br />
							<select class="browser-default" name="default_caregiver" id="defaultcaregiver">
								<option value="" disabled selected>Choose your option</option>
								<option value= "Home Health Agency">Home Health Agency</option>
								<option value= "Medical Supplier">Medical Supplier</option>
								<option value= "Social Services Agency">Social Services Agency</option>
							</select>
							
						</div>	
					</div>
					
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="button" value="Submit" class="btn cyan"  onclick="return validation();">
					</div>
				
					</div>
                    </div>
                </div>
            </div>
	</form>
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="../views/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../views/js/init.js"></script>
	<script type="text/javascript">
function goBack() {
window.history.back();
}
 $("#default_caregiver").hide();
 $('#role_name').on('change', function() {
      if ( this.value == '9')
      {
        $("#default_caregiver").show();
      }
      else
      {
        $("#default_caregiver").hide();
      }
    });


//function Validate()
//{
//var first_name = document.getElementById("first_name").value;
//var last_name = document.getElementById("last_name").value;
//var city = document.getElementById("city").value;
//var contact_number = document.getElementById("contact_number").value;
//var username = document.getElementById("username").value;
//var password = document.getElementById("password").value;
//var street_address = document.getElementById("street_address").value;
//	if(firstname != "" && last_name !="" && city !="" && contact_number != "" && username !="" && password !="" && street_address !="")
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}
function validation(){
if((document.getElementById("first_name").value).trim() == "")
{
Materialize.toast('Please enter the first name', 4000, 'red');
}
else if((document.getElementById("last_name").value).trim() == "" ){
Materialize.toast('Please enter the last name', 4000, 'red');
}
else if((document.getElementById("city").value).trim() == "" ){
Materialize.toast('Please enter the city', 4000, 'red');
}
else if((document.getElementById("state").value).trim() == ""){
Materialize.toast('Please enter the state', 4000, 'red');
}
else if((document.getElementById("zip_code").value).trim() == ""){
Materialize.toast('Please enter the zip code', 4000, 'red');
}
else if((document.getElementById("contact_number").value).trim() == ""){
Materialize.toast('Please enter the contact number', 4000, 'red');
}
else if((document.getElementById("street_address").value).trim() == ""){
Materialize.toast('Please enter the street address', 4000, 'red');
}
else if((document.getElementById("username").value).trim() == ""){
Materialize.toast('Please enter the username', 4000, 'red');
}
else if((document.getElementById("password").value).trim() == ""){
Materialize.toast('Please enter the password', 4000, 'red');
}
else if((document.getElementById("email").value).trim() == ""){
Materialize.toast('Please enter the email', 4000, 'red');
}
else if((document.getElementById("role_name").value).trim() == ""){
Materialize.toast('Please choose role name', 4000, 'red');
}



/*else if($('#service_id').find('input[name=service_id]:checked').length == "")
    {
      Materialize.toast('Please select atleast one service details', 4000, 'red');
    }
	
	else if($('#defaultvendor').find('input[name=defaultvendor]:checked').length == "")
    {
       Materialize.toast('Please select default vendor', 4000, 'red');
    }*/

else{

checkboxclick();
}

} 

function checkboxclick()
{
//alert("check");
//return false;
        $.ajax({
                type:'get',
                url:'getmessage',   
                data: {
                 email: document.getElementById("email").value,
                 username: document.getElementById("username").value,
                 contactnumber: document.getElementById("contact_number").value
                 },
                 success: function (data){
                   //alert(data);
                   if (data == 'email')
                   {Materialize.toast('Email number already exists,Kindly change different email', 4000, 'red');
                   return false;}
                   else if (data == 'username')
                   {Materialize.toast('Username already exists,Kindly change different username', 4000, 'red');
                   return false;}
                   else if (data == 'phonenumber')
                   {Materialize.toast('Phone number already exists,Kindly change different phonenumber', 4000, 'red');
                   return false;}
                   else if (data == 'sucess'){
                     add_user.submit();
                   }
                },
                error: function (xhr, textStatus, errorThrown){
                alert(errorThrown);
                }
                });   
      // return false;
}


</script>
@stop
