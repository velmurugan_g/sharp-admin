@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
		<?php use App\Http\Controllers\ApiController; use App\Http\Controllers\HospitalController;?>
		<?php $getapiid = ApiController::getAPIById($_GET["i"]);?>
		@foreach($getapiid as $key => $data)
<form action="{{ url('/updateapi') }}" method="post" class="form-horizontal" role="form">
           <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>API Management</b></h5>
					 </div>
					 
  <div class="row">			
	<div class="input-field col s6">
	   <?php $providerlist = ApiController::getproviderList();?>
		 <select class="browser-default"id="providername" name="providername" >
		 	@foreach($providerlist as $key => $providerdata)
		<option value="{{$providerdata->id}}" <?php if ($data-> provider_id==$providerdata->id) { ?>selected <?php } ?>>{{$providerdata->name}}
		</option>
	  		@endforeach
		 </select>
		 <label for="providername" class="active">Provider Name<span class="red-text">*</span></label>
	</div>	
					
	<div class="input-field col s6">
		<?php $hpllist = HospitalController::getHplDetails($data->hospital_id);?>
			@foreach($hpllist as $key => $datass)
			<input name="hospital_id" id="hospital_id" autofocus readonly="readonly"  type="text" class="validate" value="{{$datass->name}}">
			@endforeach
			<label for="email" class="active">Hospital<span class="red-text">*</span></label>
			</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="accesstoken" id="accesstoken" type="text" class="validate" value="{{$data->providername}}" >
					<label for="accesstoken" class="active">Access Token</label>
					</div>
					<div class="input-field col s6">
					<input name="accessurl" id="accessurl" type="text" class="validate" value="{{$data->access_url}}" >
					<label for="accessurl" class="active">Access URL</label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="clientid" id="clientid" type="text" class="validate" value="{{$data->client_id}}" >
					<label for="clientid" class="active">Client ID</label>
					</div>
					<div class="input-field col s6">
					<input name="secretkey" id="secretkey" type="text" class="validate" value="{{$data->secret_key}}" >
					<label for="secretkey" class="active">Secret Key</label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<textarea name="otherservices" id="otherservices" type="text" class="validate" style="border-top:none;border-left:none;border-right:none">{{$data->others}}</textarea>
					<label for="zip" class="active">Other Services</label>
					</div>
					
					</div>
					
				</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="user_id" value="<?php echo Auth::user()->id ?>">
				<input type="hidden" name="id" value="<?php echo $_GET["i"]; ?>">
					 <input type="submit" value="Submit" class="btn cyan">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>
	@endforeach
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="views/js/init.js"></script>
		<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
@stop
