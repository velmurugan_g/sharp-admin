@extends('layouts.app')

@section('content')
<div class="container m220">
        <div class="section">
		 <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <center><div class="table-header">
                           <span class="table-title">ON/ OFF</span>
							</div></center>
							</div>

							<div class="row">
                <div id="admin" class="col s12">
				@foreach($hospitalslist as $key => $data)
                    <div class="card material-table">
                        <div class="table-header">
						 
                            <span class="table-title"><a href="edithospitals?i={{$data->id}}" style="color:#0099FF">{{$data->name}}</span>
                         
                        </div><table>
                            <tbody>
								<tr>
                                <td>Owner</td><td>{{$data->owner}}</td>
                                </tr>
								<tr>
								<td>Contact Person</td>  <td>{{$data->contactperson}}</td>
                                </tr>
								<tr>
								<td>Email</td> <td>{{$data->email}}</td>
                                </tr>
								<tr>
								<td>Contact Number</td><td>{{$data->contactnumber}}</td>                                       
                            </tr>
							
                            </tbody>
                        </table>
                    </div>
                </div>
				@endforeach
            </div>
							
<div class="card material-table">
  <div class="switch">
  <h4>Caregiver</h4>
  
    <label>
      Off
      <input type="checkbox" id="mySwitch" onchange="myFunction()">
      <span class="lever"></span>
      On
    </label>
  </div>
  </div>
  </div>
  </div>
	</div>
						
		
		</div>
		</div>

@endsection

@section('scripts')

@stop