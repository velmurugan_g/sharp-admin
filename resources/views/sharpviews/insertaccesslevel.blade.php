@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <h5 align="center"><b>Access Level Details</b></h5>
					 </div>
					<div class="row">
					<div class="input-field col s6">
					<input name="access_level_name" id="access_level_name" autofocus required type="text" class="validate" value="">
					<label for="access_level_name" class="active">Access Level Name</label>
					</div>
					<div class="input-field col s6">
					<textarea name="access_level_description" id="access_level_description" type="text" class="validate" value="" autofocus style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="access_level_description" class="active">Access Level Description</label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
	  				<input type="checkbox" id="Insert" /><label for="Insert">Insert</label>&nbsp;&nbsp;
<input type="checkbox" id="Edit" /><label for="Edit">Edit</label>&nbsp;&nbsp;
<input type="checkbox" id="Update" /><label for="Update">Update</label>&nbsp;&nbsp;
<input type="checkbox" id="Delete" /><label for="Delete">Delete</label>&nbsp;&nbsp;

					<label for="middle_name" class="active">Access Level</label>
					</div>
					</div>
					
				
					
					
                     	<div class="row">
					<div class="input-field col s3 right">
					<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>
					</div>
					</div>
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="../views/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../views/js/init.js"></script>
@stop
