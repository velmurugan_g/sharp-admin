@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Users List</span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" href="{{ url ('insertuser') }}"><i class="material-icons">add</i> Add Users</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
								<th>Hospital Name</th>
								<th>Role</th>
								<!--<th>Username</th>   -->
                                <th>Email</th>
                                <th>Phone</th>
								<th>Address</th>
								<!--<th>City</th>-->								
							<!--<th>State</th>
								<th>Zipcode</th>-->
								                             
                            </tr>
                            </thead>
                            <tbody>
					<?php use App\Http\Controllers\UserController;?>
					<?php use App\Http\Controllers\HospitalController; ?>
					<?php use App\Http\Controllers\RoleController; ?>
					<?php $arr2 = UserController::getRoleID(Auth::user()->id);?>
                    <?php $userlist = UserController::getallusers();?>
	                @if ($arr2 == '1')
                        @foreach($userlist as $key => $data)
                            <tr>    
                              <td><a href="editusers?i={{$data->id}}" style="color:#0099FF">{{$data->firstname}}</a></td>
                              <td><?php echo $getrolelistview = HospitalController::getHpl($data->hospital_id); ?></td>
                        	  <td><?php echo ucfirst($getrole = RoleController::getRoleName($data->id)); ?></td>
                        	<!--  <td>{{$data->username}}</td>-->
                              <td>{{$data->email}}</td>
                              <td>{{$data->contactnumber}}</td>
                        	  <td>{{$data->streetaddress}}</td> 
                        	  <!--<td>{{$data->city}}</td>-->
                        	  
                        	<!--  <td>{{$data->state}}</td>-->
                        	   <!--<td>{{$data->zipcode}}</td> -->                
                            </tr>
                        @endforeach
                    @endif
	
                     @if ($arr2 == '2')
                     <?php $users2list = UserController::getUsers(Auth::user()->hospital_id);?>
                     @foreach($users2list as $key => $data)
                        <tr>    
                          <td>{{$data->firstname}}</td>
                    	  <td>{{$data->username}}</td>
                          <td>{{$data->email}}</td>
                          <td>{{$data->contactnumber}}</td>
                    	  <td>{{$data->streetaddress}}</td> 
                    	  <td>{{$data->city}}</td>
                    	<!--  <td>{{$data->state}}</td>
                    	   <td>{{$data->zipcode}}</td>  -->               
                        </tr>
                    @endforeach
                    	@endif
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <br><br>

    <div class="section">

    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
@stop
