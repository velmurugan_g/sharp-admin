@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	<div class="row">
    <div class="col s12">
      <ul class="tabs">
			<li class="tab col s4 default"><a href="#profile" class="active"><i class="small material-icons">perm_identity</i></a></li>
			<li class="tab col s4 default"><a href="#userlist" class=""><i class="small material-icons">contact_mail</i></a></li>
			<li class="tab col s4 default" ><a href="#vendorlist" class=""><i class="small material-icons">event_available</i></a></li>
			<li class="tab col s4 default" ><a href="#apilist" class=""><i class="small material-icons">access_time</i></a></li>
			<li class="tab col s4 default" ><a href="#switch" class=""><i class="small material-icons">swap_horiz</i></a></li>
			
		 <li class="indicator" style="left: 0px; right: 758px;"></li></ul>
    </div>
    <div id="profile" class="col s12 active" style="display: block;">
		<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
<?php use App\Http\Controllers\HospitalController;?>
		<?php $hospitalslist = HospitalController::getHplDetails($_GET["i"]); ?>
<div class="row">
                <div id="admin" class="col s12">
				@foreach($hospitalslist as $key => $data)
                    <div class="card material-table">
                        <div class="table-header">
						 
                            <span class="table-title"><a href="edithospitals?i={{$data->id}}" style="color:#0099FF">{{$data->name}}</span>
                         
                        </div><table>
                            <tbody>
								<tr>
                                <td>Owner</td><td>{{$data->owner}}</td>
                                </tr>
								<tr>
								<td>Contact Person</td>  <td>{{$data->contactperson}}</td>
                                </tr>
								<tr>
								<td>Email</td> <td>{{$data->email}}</td>
                                </tr>
								<tr>
								<td>Contact Number</td><td>{{$data->contactnumber}}</td>                                       
                            </tr>
							
                            </tbody>
                        </table>
                    </div>
                </div>
				@endforeach
            </div>
	</div>
   
	<div id="userlist" class="col s12" style="display: none;"> 
	<script type="text/javascript">
function goBack() {
window.history.back();
}
			</script>
			<div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Users List - <?php echo $getrolelistview = HospitalController::getHpl($_GET["i"]); ?></span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" href="insertuser?i=<?php echo $_GET["i"]; ?>"><i class="material-icons">add</i> Add Users</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
								<!--<th>Hospital Name</th>-->
								<th>Role</th>
								<!--<th>Username</th>   -->
                                <th>Email</th>
                                <th>Phone</th>
								<th>Address</th>
								<!--<th>City</th>-->
								
							<!--	<th>State</th>
								<th>Zipcode</th>-->
								                             
                            </tr>
                            </thead>
                            <tbody>
					<?php use App\Http\Controllers\UserController;?>
					<?php use App\Http\Controllers\RoleController; ?>
					<?php $arr2 = UserController::getRoleID(Auth::user()->id); $userslist = UserController::getUsers($_GET["i"]);?>
	 				   @if ($arr2 == '1')
                  @foreach($userslist as $key => $data)
      						<?php $getroles = RoleController::getRoleName($data->id); ?>
      						<?php if($getroles!="vendor") {?> 
                    <tr>    
                      <td><a href="editusers?i={{$data->id}}" style="color:#0099FF">{{$data->firstname}}</a></td>
                     <?php /*?> <td><?php echo $getrolelistview = HospitalController::getHpl($data->hospital_id); ?></td><?php */?>
                	  <td><?php echo ucfirst($getrole = RoleController::getRoleName($data->id)); ?></td>
                	<!--  <td>{{$data->username}}</td>-->
                      <td>{{$data->email}}</td>
                      <td>{{$data->contactnumber}}</td>
                	  <td>{{$data->streetaddress}}</td> 
                	  <!--<td>{{$data->city}}</td>-->
                	  
                	<!--  <td>{{$data->state}}</td>-->
                	   <!--<td>{{$data->zipcode}}</td> -->                
                    </tr>
	           <?php } ?>
    @endforeach
    @endif
	
 @if ($arr2 == '2')
 <?php $users2list = UserController::getUsers(Auth::user()->hospital_id);?>
 @foreach($users2list as $key => $data)
 
    <tr>    
       <td><a href="editusers?i={{$data->id}}" style="color:#0099FF">{{$data->firstname}}</a></td>
	  <td><?php echo ucfirst($getrole = RoleController::getRoleName($data->id)); ?></td>
      <td>{{$data->email}}</td>
      <td>{{$data->contactnumber}}</td>
	  <td>{{$data->streetaddress}}</td> 
	<!--  <td>{{$data->state}}</td>
	   <td>{{$data->zipcode}}</td>  -->               
    </tr>
@endforeach
	@endif
</tbody>
                        </table>
                    </div>
                </div>
            </div>
</div>
	<div id="vendorlist" class="col s12" style="display: none;"> 

<?php use App\Http\Controllers\VendorController;?>
		<?php $vendlist = VendorController::getVendorlist($_GET["i"]); $id=$_GET["i"]; ?>
			<script type="text/javascript">
function goBack() {
window.history.back();
}
			</script>
			<div class="row">
                <div id="admin" class="col s12">
				 <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Vendors List - <?php echo $getrolelistview = HospitalController::getHpl($_GET["i"]); ?></span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" href="insertvendor?i=<?php echo $_GET["i"]; ?>"><i class="material-icons">add</i> Add Users</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                        </div>
						</div>
				
					<table id="datatable2">
					<thead>
					<tr>
					<th>Name</th>
					<th>Username</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Address</th> 
					<th>Delete?</th>            
					</tr>
					</thead>
					<tbody>
					@foreach($vendlist as $key => $data)
					<tr>    
					<td><a href="editvendor?i={{$data->id}}" style="color:#0099FF">{{$data->name}}</a></td>
					<td>{{$data->username}}</td>
					<td>{{$data->email}}</td>
					<td>{{$data->phonenumber}}</td>
					<td>{{$data->address1}}</td> 
					<td class="center"><a class="btn-floating btn-small cyan" id="delete" onclick="delete_vendor({{$data->id}} )" ><i class="large material-icons">delete</i></a></td>               
					</tr>
					@endforeach
					</tbody>
					</table>
                 
                </div>
				
            </div>
	</div>
</div> <!--Vendor List Ends-->
<div id="apilist" class="col s12" style="display: none;"> 
<div class="section">
	<?php use App\Http\Controllers\ApiController;?>
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">List of API</span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" href="insertapi?i=<?php echo $_GET["i"]; ?>"><i class="material-icons">add</i> Add API</a>
                                 <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable3">
                            <thead>
                            <tr>
                                <th>Provider Name</th>
                                <th>Access Token</th>
                                <th>Client ID</th>
                                <th>Secret Key</th>
								<th>Others Services</th>
								<th>Action</th>                               
                            </tr>
                            </thead>
                            <tbody>
							
						<?php $getapidetails = ApiController::getAPI($_GET["i"]);?>			
                        @foreach($getapidetails as $key => $data)
                          <tr>    
                            <td><a href="editapi?i={{$data->id}}"style="color:#0099FF">{{$data->name}}</a></td>	 
                            <td>{{$data->access_token}}</td>
                            <td>{{$data->client_id}}</td>
                            <td>{{$data->secret_key}}</td>
                            <td>{{$data->others}}</td>
                      	  @if($data->name =='Evident')    
                      	  <td>
                      	  <a class="btn-floating btn-small cyan"  href="../public/api/patientlist?i=<?php echo $_GET["i"];?>"> <i class="large material-icons">play_arrow</i></a> 
                      	 </td>
                      	  @else
                      	 <td><a class="btn-floating btn-small cyan" onclick="inprogress()" ><i class="large material-icons">play_arrow</i></a></td> 
                      	 @endif              
                          </tr>
                   @endforeach
           </tbody>
</table>
</div>
</div>
</div>
</div>
</div><!--Start to Switvh-->
<div id="switch" class="col s12 active" style="display: none;">
<div class="row">
       <div id="admin" class="col s12">
		@foreach($hospitalslist as $key => $data)
         <div class="card material-table">
         <div class="table-header">						 
         <span class="table-title">Module  ON / OFF</span>
                         
                        </div><table>
                            <tbody>
								<tr>
                                <td>Caregiver</td>
								
								<td>
								
								 <!-- Switch -->
  							<div class="switch" >
  							 <label>
		 					  OFF
     					  <input type="checkbox" id="caregiver" value="caregiver" onchange="moduleactivate('caregiver');" <?php  if($data->caregiver_module == 1) echo "checked='checked'"?> >
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>

								</td>
                                </tr>
								<tr>
								
								<td>Treatments</td> 
								 <td>
								
								 <!-- Switch -->
  							<div class="switch" >
  							  <label>
		 					  OFF
							  <input type="checkbox" id="treatment" onchange="moduleactivate('treatment');" <?php  if($data->treatments_module == 1) echo "checked='checked'"?>>
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>
								
								</td>
                                </tr>
								<tr>
								<td>Patient Feedback & Symptoms Details</td> <td>
								
								 <!-- Switch -->
  							<div class="switch" >
  							  <label>
		 					  OFF
     						  <input type="checkbox" id="patients" onchange="moduleactivate('patients');" <?php  if($data->feedback_module == 1) echo "checked='checked'"?>	>
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>
								
								
								</td>
                                </tr>
								<tr>
								<td>Vitals & Labs</td><td>
								
								 <!-- Switch -->
  							<div class="switch" >
  							  <label>
		 					  OFF
							  	  <input type="checkbox" id="vitals" onchange="moduleactivate('vitals');" <?php  if($data->vital_lab_module == 1) echo "checked='checked'"?>	>
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>
								
								</td>                                       
                            </tr>
							<tr>
								<td>Vendors</td><td>
								 <!-- Switch -->
  							<div class="switch">
  							  <label>
		 					  OFF
     						  <input type="checkbox"  id="vendor" onchange="moduleactivate('vendor')" <?php  if($data->vendor_module == 1) echo "checked='checked'"?>>
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>
								
								
								</td>                                       
                            </tr>
							<tr>
								<td>Do you want add vital/labs into sharp score?</td><td>
								 <!-- Switch -->
  							<div class="switch">
  							  <label>
		 					  OFF
     						  <input type="checkbox"  id="vendor" onchange="moduleactivate('vital_labs')" <?php  if($data->vital_labs_score == 1) echo "checked='checked'"?>>
   							   <span class="lever"></span>
     							 ON
   								 </label>
 								 </div>
								
								
								</td>                                       
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
				@endforeach
            </div>
</div>


  </div>
</div>
      
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="js/init.js"></script>
	<script type="text/javascript">
function moduleactivate(data) {
	var values = document.getElementById('caregiver').checked;
	if(values == true){
	alert("Are you wanting enable this module?");
    var  action_key = "1";
    }
	else{
		alert("Are you wanting  disable caregiver module?");
		action_key = "0";
	}

	$.ajax({
		url: 'module_authorization',
		type: 'GET',
		data: {id:<?php echo $_GET['i']; ?>,action: action_key,type:data},
	
		success: function(response)
		{
			
		}
	});

}



function delete_vendor(id){
 $.ajax({
    url: 'deletvendor',
    type: 'GET',
    data: {hospital_id:<?php echo $_GET['i']; ?>,vendor_id: id},
    success: function(response)
    {
      alert("Vendor Deleted Sucessfully!");
     // return true;
      window.location.reload();
    }
  });
}


function inprogress(){

alert("Working in progress.....");
}

	$('#tabs').on('click','.tablink,#prodTabs a',function (e) {
    e.preventDefault();
    var url = $(this).attr("data-url");

    if (typeof url !== "undefined") {
        var pane = $(this), href = this.hash;

        // ajax load from data-url
        $(href).load(url,function(result){      
            pane.tab('show');
        });
    } else {
        $(this).tab('show');
    }
});

	</script>
@stop
