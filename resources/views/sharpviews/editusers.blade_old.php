@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
		<?php use App\Http\Controllers\UserController; use App\Http\Controllers\RoleController;?>
		<?php $getUserIDs = UserController::getUserID($_GET["i"]);?>
		@foreach($getUserIDs as $key => $data)
<form action="{{ url('/updateuser') }}" method="post" class="form-horizontal" role="form" name="edit_user">
<input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					   <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>User Details</b></h5>
					 </div>
					<div class="row">
					<div class="input-field col s6">
					<input name="first_name" id="first_name" autofocus required type="text" class="validate" value="{{$data->firstname}}">
					<label for="first_name" class="active">First Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="last_name" id="last_name"  required type="text" class="validate" value="{{$data->lastname}}">
					<label for="last_name" class="active">Last Name<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="middle_name" id="middle_name"  type="text" class="validate" value="{{$data->middlename}}" >
					<label for="middle_name" class="active">Middle Name</label>
					</div>
					<div class="input-field col s6">
					<input name="city" id="city" type="text" required class="validate" value="{{$data->city}}" >
					<label for="state" class="active">City<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="state" id="state" type="text" required class="validate" value=" {{$data->state}}" >
					<label for="state" class="active">State<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="zip_code" id="zip_code" type="text" maxlength="5" required class="validate" value="{{$data->zipcode}}" onKeyPress="return isNumberKey(event)">
					<label for="zip_code" class="active">Zip Code<span class="red-text">*</span></label>
					</div>
					</div>
		
					
					<div class="row">
						<div class="input-field col s6">
							<input name="contact_number" id="contact_number" type="text" class="validate"  maxlength="10" required value="{{$data->contactnumber}}" onKeyPress="return isNumberKey(event)">
							<label for="contact_number" class="active">Contact Number<span class="red-text">*</span></label>
						</div>
						<div class="input-field col s6">
							<textarea name="street_address" id="street_address" type="text" class="validate" required value=""  style="border-top:none;border-left:none;border-right:none">{{$data->streetaddress}}</textarea>
							<label for="street_address" class="active">Street Address<span class="red-text">*</span></label>
						</div>
					</div>
		<?php $getrolelistss = RoleController::getRolesAll(); $getroles = RoleController::getRoleName($data->id); $roleval = $getroles;?>
					<div class="row">
						
						<div class="input-field col s6">
							<input name="email" id="email" type="text" required class="validate" value="{{$data->email}}" >
							<label for="street_address" class="active">E-mail<span class="red-text">*</span></label>
						</div>
						<div class="input-field col s6">
							<label for="hospital_name" class="active" style="font-size:1em">Role Name <span class="red-text">*</span></label><br />
								<select class="browser-default" name="role_id" id="role_name">
								 <option value="" disabled selected><?php  echo ucfirst($getroles); ?></option>
								@foreach($getrolelistss as $key => $datas)
								<?php if($roleval!=$datas->rolename) ?>
								<option value= "{{$datas->id}}">{{$datas->rolename}}</option>
								@endforeach
								</select>
							</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<input name="username" id="username" required readonly type="text" class="validate" value="{{$data->username}}" >
							<label for="username" class="active">User Name<span class="red-text">*</span></label>
						</div>
						<div class="input-field col s6">
							<input name="password" id="password" required type="password" class="validate" value="{{$data->password}}" >
							<label for="street_address" class="active">Password<span class="red-text">*</span></label>
						</div>
					</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
					<input type="hidden" name="id" value="<?php echo $_GET["i"]; ?>">
					<input type="hidden" name="hospital_id" value="{{$data->hospital_id}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">					
					<input type="button" value="Submit" class="btn cyan" onclick="edit_user_checkclick();">
					</div>
				
					</div>
                    </div>
                </div>
            </div>
	</form>
	@endforeach
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
 <!--   <script type="text/javascript" src="../views/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../views/js/init.js"></script>-->
<script type="text/javascript">

function edit_user_checkclick(){
if((document.getElementById("first_name").value).trim() == "")
{
Materialize.toast('Please enter the first name', 4000, 'red');
}
else if((document.getElementById("last_name").value).trim() == "" ){
Materialize.toast('Please enter the last name', 4000, 'red');
}
else if((document.getElementById("city").value).trim() == "" ){
Materialize.toast('Please enter the city', 4000, 'red');
}
else if((document.getElementById("state").value).trim() == ""){
Materialize.toast('Please enter the state', 4000, 'red');
}
else if((document.getElementById("zip_code").value).trim() == ""){
Materialize.toast('Please enter the zip code', 4000, 'red');
}
else if((document.getElementById("contact_number").value).trim() == ""){
Materialize.toast('Please enter the contact number', 4000, 'red');
}
else if((document.getElementById("street_address").value).trim() == ""){
Materialize.toast('Please enter the street address', 4000, 'red');
}

else if((document.getElementById("password").value).trim() == ""){
Materialize.toast('Please enter the password', 4000, 'red');
}
else if((document.getElementById("email").value).trim() == ""){
Materialize.toast('Please enter the email', 4000, 'red');
}
else if((document.getElementById("role_name").value).trim() == ""){
Materialize.toast('Please choose role name', 4000, 'red');
}

/*else if($('#service_id').find('input[name=service_id]:checked').length == "")
    {
      Materialize.toast('Please select atleast one service details', 4000, 'red');
    }
	
	else if($('#defaultvendor').find('input[name=defaultvendor]:checked').length == "")
    {
       Materialize.toast('Please select default vendor', 4000, 'red');
    }*/

else{
edituser_mail_check();
}

} 

function edituser_mail_check()
{
//alert("check");
//return false;
        $.ajax({
                type:'get',
                url:'checkuseremail',   
                data: {
                 email: document.getElementById("email").value, 
				 user_id: document.getElementById("id").value,  				
				 username: document.getElementById("username").value,			 
                 contactnumber: document.getElementById("contact_number").value
                 },
                 success: function (data){
                   //alert(data);
                   if (data == 'email')
                   {Materialize.toast('Email already exists,Kindly change different email', 4000, 'red');
                   return false;}
                   else if (data == 'phonenumber')
                   {Materialize.toast('Phone number already exists,Kindly change different phonenumber', 4000, 'red');
                   return false;}
                   else if (data == 'sucess'){
                     edit_user.submit();
                   }
                },
                error: function (xhr, textStatus, errorThrown){
                alert(errorThrown);
				return false;
                }
                });   
      // return false;
}




function goBack() {
window.history.back();
}
</script>
@stop
