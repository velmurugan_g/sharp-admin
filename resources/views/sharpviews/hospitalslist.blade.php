@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">List of Hospitals</span>
                            <div class="actions">
							<a class="btn-floating btn-small cyan" href="{{ url ('inserthospital') }}"><i class="material-icons">add</i> Add Hospital</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Owner</th>
                                <th>Contact Person</th>
                                <th>Email</th>
                                <th>Contact Number</th>                                
                            </tr>
                            </thead>
                            <tbody>
                      <!--  <?php use App\Http\Controllers\HospitalController;?>
                       <?php $hospitals = HospitalController::getHplList(); ?> -->
                        @foreach($hospitalslist as $key => $data)
                            <tr>    
                              <td><a href="hospitals?i={{$data->id}}" style="color:#0099FF">{{$data->name}}</a></td>
                              <td>{{$data->owner}}</td>
                              <td>{{$data->contactperson}}</td>
                              <td>{{$data->email}}</td>
                              <td>{{$data->contactnumber}}</td>                 
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
@stop
