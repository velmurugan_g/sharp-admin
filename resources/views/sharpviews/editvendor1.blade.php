@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
		<?php use App\Http\Controllers\VendorController;?>
		<?php $getUserIDs = VendorController::getUserID($_GET["i"]);?>
		@foreach($getUserIDs as $key => $data)
<form action="{{ url('/updatevendor') }}" method="post" class="form-horizontal" role="form">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					 <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>Edit Vendor Details</b></h5>
					 </div>
					 
					<div class="row">
					<div class="input-field col s6">
					<input name="vendorname" id="vendorname" autofocus required type="text" class="validate" value="{{$data->name}}">
					<label for="vendorname" class="active">Vendor Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="email" id="email"  required type="text" class="validate" value="{{$data->email}}">
					<label for="email" class="active">Email id<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="username" id="username" type="text" class="validate" value="{{$data->username}}" >
					<label for="username" class="active">Username</label>
					</div>
					<div class="input-field col s6">
					<input name="password" id="password" type="password" class="validate" value="{{$data->password}}" >
					<label for="password" class="active">Password</label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="city" id="city" type="text" class="validate" value="{{$data->city}}" >
					<label for="city" class="active">City</label>
					</div>
					<div class="input-field col s6">
					<input name="state" id="state" type="text" class="validate" value="{{$data->state}}" >
					<label for="state" class="active">State</label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="zip" id="zip" type="text" class="validate" value="{{$data->zip}}" onKeyPress="return isNumberKey(event)">
					<label for="zip" class="active">Zip</label>
					</div>
					<div class="input-field col s6">
					<input name="phonenumber" id="phonenumber" type="text" class="validate" value="{{$data->phonenumber}}" onKeyPress="return isNumberKey(event)">
					<label for="phonenumber" class="active">Contact Number</label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
						<textarea name="streetaddress1" id="streetaddress1" type="text" class="validate" value="{{$data->address1}}"  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="streetaddress1" class="active">Street Address 1</label>
					</div>
					<div class="input-field col s6">
					<textarea name="streetaddress2" id="streetaddress2" type="text" class="validate" value="{{$data->address2}}"  style="border-top:none;border-left:none;border-right:none"></textarea>
					<label for="streetaddress2" class="active">Street Address 2</label>
					</div>
					</div>
					
					<h6>Service Details</h6>
			<div class="row">
			<div class="input-field col s6">
		                                <?php $adminlist = VendorController::getAdminVendorServices();?>
						
							<select class="browser-default" name="service_id">
								@foreach($adminlist as $key => $datas)
								@if($datas->id == $data->service_id)
								<option value="{{$datas->id}}">{{$datas->services}}</option>
								@endif
								@endforeach
							</select>
																
								</div>
								<div class="input-field col s6">
								<p>
									  <label>
										<input id="defaultvendor" name="defaultvendor" type="checkbox" <?php if($data->isdefault==1) echo "checked='checked'"; ?> class="filled-in"><span style="line-height:16px;">Default Vendor</span> 
									  </label>
									</p>
								</div>
			</div>
				<div class="row">
				<div class="input-field col s6">
				    <label for="managepatients">How many Patients will manage in a Single day ?</label>
					<input name="managepatients" id="managepatients" type="text" class="validate" value="{{$data->patient_manage}}" onKeyPress="return isNumberKey(event)">
					
					</div>
					<div class="input-field col s6">
					<label for="phonenumber">Quality Level</label>
					<input name="qualitylevel" id="qualitylevel" type="text" class="validate" value="{{$data->quality_level}}" onKeyPress="return isNumberKey(event)">
					
					</div>
			</div>
						
							
					<div class="row">
						<div class="input-field col s8">
						<h6>Map Plotting</h6>
						<button id="search" class="search_button">Search</button>
						<button id="clear_button" class="clear_button">Clear</button>
<hr>
<div id="map-canvas"></div>

												</div>
												<div class="input-field col s4">
												<h6>Coverage Area</h6>
<textarea id="zipcodes" name="zipcodes" style="height:360px;">{{$data->coveragearea}}</textarea>
												</div>
							
						</div>
					<h6>Days Available</h6>
					<div class="row blue-text">
									      <div class="col l1"></div><div class="col l1"></div>
										  
										 
										   <!--<div class="col s6 m4 l1">
											 <label><input id="alls" name="alls" type="checkbox" value="all" class="filled-in" onclick="toggle(this);"><span class="green-text"><b>ALL</b></span> </label>
											</div>-->
											
										    <div class="col s6 m4 l1">
											 <label><input id="Sunday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Sunday"); if($adlist =="Sunday") echo "checked='checked'"?> value="Sunday" class="filled-in"><span class="green-text"><b>Sun</b></span> </label>
											 </div>
											 
											 <div class="col s6 m4 l1">
											 <label><input id="Monday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Monday"); if($adlist =="Monday") echo "checked='checked'"?> value="Monday" class="filled-in"><span class="green-text"><b>Mon</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Tuesday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Tuesday"); if($adlist =="Tuesday") echo "checked='checked'"?> value="Tuesday" class="filled-in"><span class="green-text"><b>Tue</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Wednesday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Wednesday"); if($adlist =="Wednesday") echo "checked='checked'"?> value="Wednesday" class="filled-in"><span class="green-text"><b>Wed</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Thursday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Thursday"); if($adlist =="Thursday") echo "checked='checked'"?> value="Thursday" class="filled-in"><span class="green-text"><b>Thu</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Friday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Friday"); if($adlist =="Friday") echo "checked='checked'"?> value="Friday" class="filled-in"><span class="green-text"><b>Fri</b></span> </label>
											 </div>
											 <div class="col s6 m4 l1">
											 <label><input id="Saturday" name="weekdayadd[]" type="checkbox" <?php $adlist = VendorController::getAvailRecords($data->id,"Saturday"); if($adlist =="Saturday") echo "checked='checked'"?> value="Saturday" class="filled-in"><span class="green-text"><b>Sat</b></span> </label>
											 </div>
										
										</div>
										<h6>Tasks Available</h6>
										
		                                <?php $vendlist = VendorController::getTasklookup();?>
										<div class="row">
									@foreach($vendlist as $key => $datas)
									<p class="input-field col s6 m6 l3" style="padding-bottom:7px;">
									  <label>
										<input id="{{$datas->Id}}" name="availabletime[]" type="checkbox" <?php $dadlist = VendorController::gettimeRecords($data->id,$datas->Id); if($dadlist==$datas->Id) echo "checked='checked'"?> value="{{$datas->Id}}" class="filled-in" /><span style="line-height:16px;">{{$datas->Task}}</span>
									  </label>
									</p>
									@endforeach
								
									
									</div>
					
                     	<div class="row">
					<div class="input-field col s3 right">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="vendor_id" value="<?php echo $_GET["i"]; ?>">
				<input type="hidden" name="user_id" value="<?php echo Auth::user()->id ?>">
				<input type="hidden" name="hospital_id" value="{{$data->hospital_id}}">
					 <input type="submit" value="Submit" class="col s12 btn btn-large waves-effect cyan">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>
			@endforeach
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
			<script type="text/javascript">
function goBack() {
window.history.back();
}
</script>
<style>
body {
	font-family : helvetica, arial;
}
button {
	padding: 5px;
	font-size: 120%;
}
.search_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 9999;
	right: 100px;
	top: 49px;
}
.clear_button
{
	padding: 5px;
	background: #00bcd4;
	border: 1px solid #fff; 
	padding: 10px;
	color: #fff;
	font-size: 14px;
	position:absolute;
	z-index: 9999;
	right:164px;
	top: 49px;
}
/*textarea {
	float: left;
	clear: none;
	width: 200px;
	height: 360px;
	margin-left: 10px;
}*/
#map-canvas {
	width:700px;
	height:400px;
	float: left;
	clear: none;
}
</style>
<script type="text/javascript">
var usaCenter = new google.maps.LatLng(39.8106460, -98.5569760);
var poly, map;
var markers = [];
var path = new google.maps.MVCArray;

function createMap() {
	map = new google.maps.Map(document.getElementById("map-canvas"), {
		center: usaCenter,
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		zoomControl: true,
		streetViewControl: false,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		}
	});

	poly = new google.maps.Polygon({
		strokeWeight: 1,
		strokeColor: '#ff0000', 
		fillColor: '#ff0000'
	});
	poly.setMap(map);
	poly.setPaths(new google.maps.MVCArray([path]));

	google.maps.event.addListener(map, 'click', addPolygonPoint);
}

function addPolygonPoint(event) {
	path.insertAt(path.length, event.latLng);
	var marker = new google.maps.Marker({
		icon : 'csquare.png',
		position: event.latLng,
		map: map,
		draggable: true
	});
	markers.push(marker);
	google.maps.event.addListener(marker, 'click', function() {
		marker.setMap(null);
		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		markers.splice(i, 1);
		path.removeAt(i);
	});
	google.maps.event.addListener(marker, 'dragend', function() {
		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		path.setAt(i, marker.getPosition());
	});
}

google.maps.event.addDomListener(window, 'load', createMap);

$("#search").click(function() {
	var latLngs = '';
	for (var i=0;i<markers.length;i++) {
		var ll=markers[i].getPosition().toUrlValue(4);
		latLngs+='['+ll.toString()+']';
	}
	var url='http://localhost:90/sharpadminui3/zip/zip.php?latlngs='+latLngs;
	$.ajax({
		url: url, 
		success : function(response) {
			$('#zipcodes').text(response);
		}
	});
	return false;
});

</script>

@stop
