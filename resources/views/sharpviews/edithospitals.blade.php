@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
		<?php use App\Http\Controllers\HospitalController;?>
		<?php $getHplDetails = HospitalController::getHplDetails($_GET["i"]); ?>
		@foreach($getHplDetails as $key => $data)
<form action="{{ url('/updatehospital') }}" method="post" class="form-horizontal" role="form" name="edit_hospital">
<input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="material-table">
                     <div class="row">
					  <i class="small material-icons" onclick="goBack()" style="cursor:pointer;float:left;">arrow_back</i>
					 <h5 align="center"><b>Hospital Details</b></h5>
					 </div>
					 
					<div class="row">
					<div class="input-field col s6">
					<input name="name" id="name" autofocus  type="text" class="validate" value="{{$data->name}}">
					<label for="name" class="active">Hospital Name<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="city" id="city" autofocus  type="text" class="validate" value="{{$data->city}}">
					<label for="city" class="active">City<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="owner" id="owner" type="text"   class="validate" value="{{$data->owner}}" autofocus>
					<label for="owner" class="active">Owner of Hospital<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="state" id="state" type="text"  class="validate" value="{{$data->state}}" autofocus>
					<label for="state" class="active">State<span class="red-text">*</span></label>
					</div>
					</div>
					<div class="row">
					<div class="input-field col s6">
					<input name="contactperson" id="contactperson"  type="text" class="validate" value="{{$data->contactperson}}" autofocus>
					<label for="contact_person" class="active">Contact Person<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="zip" id="zip" maxlength="5" type="text"  class="validate" value="{{$data->zip}}"  onKeyPress="return isNumberKey(event)" autofocus>
					<label for="zip" class="active">Zip Code<span class="red-text">*</span></label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="email" id="email" type="text"  class="validate" value="{{$data->email}}" autofocus >
					<label for="email" class="active">Contact Email<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<input name="weburl" id="web_url" type="text" class="validate" value="{{$data->weburl}}" autofocus>
					<label for="weburl" class="active">Web URL</label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
					<input name="contactnumber" id="contact_number" maxlength="11"  type="text" class="validate" value="{{$data->contactnumber}}" autofocus maxlength="10" onKeyPress="return isNumberKey(event)">
					<label for="contactnumber" class="active">Contact Number<span class="red-text">*</span></label>
					</div>
					<div class="input-field col s6">
					<textarea name="streetaddress" id="street_address"  type="text" class="validate" value="" autofocus style="border-top:none;border-left:none;border-right:none">{{$data->streetaddress}}</textarea>
					<label for="streetaddress" class="active">Street Address<span class="red-text">*</span></label>
					</div>
					</div>
					
						<div class="row">
					<div class="input-field col s6">
					<input name="npi" id="npi" type="text" class="validate" value="{{$data->npi}}" autofocus>
					<label for="npi" class="active">NPI</label>
					</div>
					<div class="input-field col s6">
					<input name="medicareid" id="medicareid" type="text" class="validate" value="{{$data->medicareid}}" autofocus>
					<label for="medicareid" class="active">Medicare ID</label>
					</div>
					</div>
						<div class="row">
					<div class="input-field col s6">
					<input name="medicareprovider" id="medicareprovider" type="text" class="validate" value="{{$data->medicareprovider}}" autofocus>
					<label for="medicareprovider" class="active">Medicare Provider</label>
					</div>
					<div class="input-field col s6">
					<textarea name="description" id="description" type="text" class="validate" value="" autofocus style="border-top:none;border-left:none;border-right:none">{{$data->description}}</textarea>
					<label for="description" class="active">Description</label>
					</div>
					</div>
                     	<div class="row">
					<div class="input-field col s12 center">
					<!--<button type="button" name="btnlogin" class="col s12 btn btn-large waves-effect cyan">Submit</button>-->
				<input type="hidden" name="id" value="<?php echo $_GET["i"] ?>">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="button" value="Submit" class="btn cyan" onclick="edit_hospital_checkclick();">
					</div>
					</div>
                    </div>
                </div>
            </div>
			</form>
@endforeach
        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="../public/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../public/js/init.js"></script>
<script type="text/javascript">

function edit_hospital_checkclick(){
if((document.getElementById("name").value).trim() == "")
{
Materialize.toast('Please enter the hospital name', 4000, 'red');
}
else if((document.getElementById("owner").value).trim() == "" ){
Materialize.toast('Please enter the owner of hospital', 4000, 'red');
}
else if((document.getElementById("city").value).trim() == "" ){
Materialize.toast('Please enter the city', 4000, 'red');
}
else if((document.getElementById("state").value).trim() == ""){
Materialize.toast('Please enter the state', 4000, 'red');
}
else if((document.getElementById("contactperson").value).trim() == ""){
Materialize.toast('Please enter the contact person', 4000, 'red');
}

else if((document.getElementById("zip").value).trim() == ""){
Materialize.toast('Please enter the zip code', 4000, 'red');
}
else if((document.getElementById("email").value).trim() == ""){
Materialize.toast('Please enter the contact email', 4000, 'red');
}
else if((document.getElementById("contact_number").value).trim() == ""){
Materialize.toast('Please enter the contact number', 4000, 'red');
}
else if((document.getElementById("street_address").value).trim() == ""){
Materialize.toast('Please enter the street address', 4000, 'red');
}




/*else if($('#service_id').find('input[name=service_id]:checked').length == "")
    {
      Materialize.toast('Please select atleast one service details', 4000, 'red');
    }
	
	else if($('#defaultvendor').find('input[name=defaultvendor]:checked').length == "")
    {
       Materialize.toast('Please select default vendor', 4000, 'red');
    }*/

else{
edithospital_mail_check();
}

} 


function edithospital_mail_check()
{
//alert("check");
//return false;
        $.ajax({
                type:'get',
                url:'checkhospitalemail',   
                data: {
                 email: document.getElementById("email").value, 
				 user_id: document.getElementById("id").value,  				
				 username: document.getElementById("name").value,			 
                 contactnumber: document.getElementById("contact_number").value
                 },
                 success: function (data){
                   //alert(data);
                   if (data == 'email')
                   {Materialize.toast('Email already exists,Kindly change different email', 4000, 'red');
                   return false;}
                   else if (data == 'phonenumber')
                   {Materialize.toast('Phone number already exists,Kindly change different phonenumber', 4000, 'red');
                   return false;}
                   else if (data == 'sucess'){
                     edit_hospital.submit();
                   }
                },
                error: function (xhr, textStatus, errorThrown){
                alert(errorThrown);
				return false;
                }
                });   
      // return false;
}





function goBack() {
window.history.back();
}
</script>
@stop
