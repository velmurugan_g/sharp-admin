@extends('layouts.app')

@section('content')
    <div class="container m220">
        <div class="section">
	
            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">List of Roles</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div><table id="datatable">
                            <thead>
                            <tr>
                                <th>Role Name</th>
								<!--<th>Description</th>   
                                <th>Access Level</th>-->
                                <th>Createdby</th>								                             
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($roleslist as $key => $data)
    <tr>    
      <td>{{$data->rolename}}</td>
	  <!--<td>{{$data->description}}</td>
      <td>{{$data->accesslevel}}</td>-->
      <td>Super-admin</td>               
    </tr>
@endforeach
</tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
@stop
