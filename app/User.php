<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = "admin_user_entity";
	 
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];
/* protected $fillable = ['firstname', 'lastname', 'middlename', 'gender', 'dateofbirth','hospital_id','photo','contactnumber','email','city','state','zipcode','streetaddress','address2','username','password'];*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'password', 'remember_token',
    ];
	
	 public function getAuthPassword()
    {
      return $this->password;
    }
		 /*'id','password_key','access_token','secretquestion','secretanswer','npi','refhospital','status','Invited','registered','pin_number','createdby','enable','Deleted','Password_Reset_Timing','record_created_date','record_updated_date'*/
}
