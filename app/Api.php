<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_api';
    protected $fillable = ['providername', 'hospital_id', 'access_token','access_url','client_id','secret_key','others','accessible_id','updated_at','created_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];
}
