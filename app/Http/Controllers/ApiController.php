<?php

namespace App\Http\Controllers;

use App\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

class ApiController extends Controller{
	
	protected $apinames;
	
	public function __construct() {
		$this->middleware('auth');
	}
 
    public function index(){ 
		$Apis  = Api::all();
        return response()->json($Apis);
    }
	
	 public function createApi(Request $request){
		$api = new Api;
		
		if($request->get('providername') != '')
		{
		
		
			$getprovidername = DB::select('select name from admin_providername_lookup where id='.$request->get('providername'));
			foreach($getprovidername as $value){
			$getprovider = $value->name ;
			}
			//$name = $getprovider;
			$api->providername = $getprovider;
			$api->provider_id =$request->get('providername');
			$api->hospital_id =$request->get('hospital_id');
			$api->client_id = $request->get('clientid');
			$api->access_token =$request->get('accesstoken');
			$api->access_url = $request->get('accessurl');
			$api->secret_key =$request->get('secretkey');
			$api->others = $request->get('otherservices');
			$api->accessible_id= $request->get('accessmethods');
			$api->save();
			
		}
		
		$cal = date("Y-m-d");
		/*if($request->get('providername') != '')
		{
		$geturl = DB::insert('insert into admin_api (`providername`, `hospital_id`, `client_id`, `access_token`, `access_url`, `secret_key`, `others`, `accessible_id`, `updated_at`, `created_at`) values (?,?,?,?,?,?,?,?,?,?)', ['"'.$request->get('providername').'"',$request->get('hospital_id'),'"'.$request->get('clientid').'"','"'.$request->get('accesstoken').'"','"'.$request->get('accessurl').'"','"'.$request->get('secretkey').'"','"'.$request->get('otherservices').'"',$request->get('accessmethods'),'"'.$cal.'"','"'.$cal.'"']);
		}*/
     //return redirect()->back();
	  return redirect('apilist?i='.$request->get('hospital_id'));
    }
	
		public function updateApi(Request $request){
		$id = $request->get('id');
		$api  = Api::find($id);	
		$getprovidername = DB::select('select name from admin_providername_lookup where id='.$request->get('providername'));
		foreach($getprovidername as $value)
		   {
		$getprovider = $value->name ;
			}			
			$api->providername = $getprovider;			
			$api->provider_id = $request->get('providername');
			$api->client_id = $request->get('clientid');
			$api->access_token =$request->get('accesstoken');
			$api->access_url = $request->get('accessurl');
			$api->secret_key =$request->get('secretkey');
			$api->others = $request->get('otherservices');			
			$api->save();	
			//dump($api->hospital_id);
			//die;		
	 return redirect('apilist?i='.$api->hospital_id);
	// return redirect('apilist?i='.$request->get('id'));
    }
		public static function getAccessMethods(){ 
		$getRoles = DB::select('select id,method_name from admin_accessiblemethods');
		return $getRoles;
    }
	
		public static function getAPI($hospital_id){ 
		$getapidetails = DB::select('select admin_api.*,admin_providername_lookup.id as admin_providername_lookup_id,admin_providername_lookup.name from admin_api left join 		admin_providername_lookup on admin_providername_lookup.id=admin_api.provider_id where admin_api.hospital_id='.$hospital_id);
		
		/* foreach ($getrole_id as $getrole) {
		  $role_id = $getrole->role_id;
		 }*/
		return $getapidetails;
		
    }
	
	public static function getproviderList()
	{ 
		$providerlist = DB::select('select * from admin_providername_lookup');
		return $providerlist;
	}
	
	
	public static function getAPIById($id){ 
		$getapi_id = DB::select('select admin_api.*,admin_providername_lookup.id as admin_providername_lookup_id,admin_providername_lookup.name from admin_api left join 		admin_providername_lookup on admin_providername_lookup.id=admin_api.provider_id  where admin_api.id='.$id);
		return $getapi_id;
    }


}