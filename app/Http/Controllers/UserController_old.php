<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Encryption\Encrypter;
/*use Illuminate\Support\Facades\Mail;*/

use App\Http\Requests;

class UserController extends Controller{

public $data;
    	public function __construct() {
		$this->middleware('auth');
	}
 
    public function index(){ 
		$User  = User::all();
        return response()->json($User);
    }
	
	public function generateRandomString($length = 30) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// To Create a New User

	 public function createUser(Request $request){
		$user = new User;
		if($request->get('first_name') != '')
		{
			$user->firstname =$request->get('first_name');
			$user->lastname = $request->get('last_name');
			$user->middlename = $request->get('middle_name');
			$user->city = $request->get('city');
			$user->state = $request->get('state');
			$user->zipcode = $request->get('zip_code');
			$user->email = $request->get('email');
			$user->gender = $request->get('gender');
			$user->contactnumber = $request->get('contact_number');
			$user->streetaddress = $request->get('street_address');
			$user->hospital_id = $request->get('hospital_id');
			$user->username = $request->get('username');
			$user->password = hash("sha1",$request->get('password'));
			$user->email = $request->get('email');
			$user->default_caregiver = $request->get('default_caregiver');
			$user->access_token = $this->generateRandomString();
			$user->status ='active';
			$user->save();
		}
			$geturl = DB::insert('insert into admin_user_roles_entity (user_id, role_id) values (?, ?)', [$user->id, $request->get('role_id')]);
			return redirect('hospitals?i='.$request->get('hospital_id'));
    }
	
	public function updateUser(Request $request){
	$id = $request->get('id');
		$user  = User::find($id);
			$user->firstname =$request->get('first_name');
			$user->lastname = $request->get('last_name');
			$user->middlename = $request->get('middle_name');
			$user->city = $request->get('city');
			$user->state = $request->get('state');
			$user->zipcode = $request->get('zip_code');
			$user->email = $request->get('email');
			$user->contactnumber = $request->get('contact_number');
			$user->streetaddress = $request->get('street_address');
			$user->password = hash("sha1",$request->get('password'));
			$user->save();
			if($request->get('role_id') != "")
			$updateurl = DB::update('update admin_user_roles_entity set role_id = '.$request->get('role_id').' where user_id = '.$request->get('id'));
	 return redirect('hospitals?i='.$request->get('hospital_id'));
    }
	
	 public static function getRoleID($userid){ 
	 static $roles_id;
		$getrole_id = DB::select('select role_id from admin_user_roles_entity where user_id='.$userid);
		 foreach ($getrole_id as $getrole) {
		  $roles_id = $getrole->role_id;
		 }
		return $roles_id;
    }
	 public static function getUserID($userid){ 
		$getrole_id = DB::select('select * from admin_user_entity where id='.$userid);
		return $getrole_id;
    }
	
	public static function getTotalUser(){ 
		$getuser_val = DB::select('select distinct(count(*)) as usercnt from admin_user_entity');
		 foreach ($getuser_val as $getusr) {
		  $getusrcnt = $getusr->usercnt;
		 }
		return $getusrcnt;
    }
	
	public static function getTotalUser2($hospital_id){ 
		$getuser_val = DB::select('select distinct(count(*)) as usercnt from admin_user_entity where hospital_id='.$hospital_id);
		 foreach ($getuser_val as $getusr) {
		  $getusrcnt = $getusr->usercnt;
		 }
		return $getusrcnt;
    }
	
	public static function getUsers($hospital_id){ 
		$gethospital = DB::select('select * from admin_user_entity where hospital_id='.$hospital_id.' and Deleted <> 1');
		/* foreach ($getrole_id as $getrole) {
		  $role_id = $getrole->role_id;
		 }*/
		return $gethospital;
    }
	
	public function username_exists(Request $request)
    {
        $mail = $request->get('email');
        $username = $request->get('username');
        $contactnumber = $request->get('contactnumber');      
        $emails = DB::select('select email from admin_user_entity where email="'.$mail.'"');
        $usernames = DB::select('select email from admin_user_entity where username="'.$username.'"');
        $phonenumbers = DB::select('select email from admin_user_entity where contactnumber="'.$contactnumber.'"');
        //echo count($users);die;
          if (count($emails)> 0)  
          {$mssg = "email";}
           elseif (count($usernames)> 0)  
          {$mssg = "username"; }
           elseif (count($phonenumbers)> 0)  
          {$mssg = "phonenumber"; }
          else
         {$mssg = "sucess"; }
         return $mssg;
     }
	 
	 public function checkuser_email_exist(Request $request)
    {
	    $mail = $request->get('email');
        $username = $request->get('username');
        $contactnumber = $request->get('contactnumber');      
		$user_id = 	$request->get('user_id'); 	
        $emails = DB::select('select * from admin_user_entity where email="'.$mail.'"');
        $usernames = DB::select('select * from admin_user_entity where username="'.$username.'"');
        $phonenumbers = DB::select('select * from admin_user_entity where contactnumber="'.$contactnumber.'"');
       // echo count($emails);die;
		 $mssg = "sucess";
			foreach ($emails as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "email";
			  break;
			 }
			 foreach ($usernames as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "username";
			  break;
			 }
			 foreach ($phonenumbers as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "phonenumber";
			  break;
			 }
		    return $mssg;
     } 
	 
	 
/*	public function mailsend(){ 
		$data = array('name'=>"Radha Krishnan");   
      Mail::send("mail", $data, function($message) {
         $message->to('radhakrishnan@msitl.com', 'Laravel')->subject
            ('Laravel Basic Testing Mail');
         $message->from('sharpkatenterprise@gmail.com','Radha Krishnan');
      });
	}*/
}