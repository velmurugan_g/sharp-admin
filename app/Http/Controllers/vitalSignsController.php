<?php


namespace App\Http\Controllers;

use App\admin_vitals_raw_data;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class vitalSignsController extends Controller{

	public function __construct() {
	
	}
	
	
	 public function vitallist(){
	 
 $json ='{
  "resourceType":"Bundle",
  "link":[
    {
      "relation":"self",
      "url":"https://<base>/fhir/dstu2/Observation?value=gt103"
    },
    {
      "relation":"next",
      "url":"https://<base>/fhir/dstu2/Observation?value=gt103&page=2"
    }
  ],
  "entry":[
    {
      "fullUrl":"https://<base>/fhir/dstu2/Observation/id-vs.ip2.WT.1.0.357594.0.-1.0",
      "resource":{
        "resourceType":"Observation",
        "id":"id-vs.ip2.WT.1.0.357594.0.-1.0",
        "status":"final",
        "category":{
          "coding":[
            {
              "system":"http://hl7.org/fhir/observation-category",
              "code":"vital-signs",
              "display":"Vital Signs"
            }
          ]
        },
        "code":{
          "coding":[
            {
              "system":"LOINC",
              "code":"29463-7",
              "display":"Weight"
            }
          ],
          "text":"Weight"
        },
        "subject":{
          "reference":"Patient/id-0.1.581"
        },
        "encounter":{
          "reference":"Encounter/id-1.357594"
        },
        "effectiveDateTime":"2016-04-16T22:54:00-05:00",
        "issued":"2016-04-16T22:54:00.000-05:00",
        "valueQuantity":{
          "value":190,
          "unit":"lbs",
          "system":"http://unitsofmeasure.org",
          "code":"[lb_av]"
        }
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Observation/id-res.res..1.0.20000071.2865274.1.1400266020000",
      "resource":{
        "resourceType":"Observation",
        "id":"id-res.res..1.0.20000071.2865274.1.1400266020000",
        "status":"final",
        "category":{
          "coding":[
            {
              "system":"http://hl7.org/fhir/observation-category",
              "code":"laboratory",
              "display":"Laboratory"
            }
          ]
        },
        "code":{
          "coding":[
            {
              "system":"LOINC",
              "code":"14749-6",
              "display":"GLUCOSE"
            }
          ],
          "text":"GLUCOSE"
        },
        "subject":{
          "reference":"Patient/id-1.1.464"
        },
        "encounter":{
          "reference":"Encounter/id-1.20000071"
        },
        "effectiveDateTime":"2014-06-16T13:47:00-05:00",
        "issued":"2014-05-16T13:47:00.000-05:00",
        "valueQuantity":{
          "value":90,
          "unit":"mg/dl",
          "system":"http://unitsofmeasure.org"
        },
        "referenceRange":[
          {
            "text":"L=70       H=110"
          }
        ]
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Observation/id-smoke.ip0.T.1.0.357620.0.0.0",
      "resource":{
        "resourceType":"Observation",
        "id":"id-smoke.ip0.T.1.0.357620.0.0.0",
        "status":"final",
        "category":{
          "coding":[
            {
              "system":"http://hl7.org/fhir/observation-category",
              "code":"social-history",
              "display":"Social History"
            }
          ]
        },
        "code":{
          "coding":[
            {
              "system":"LOINC",
              "code":"3924-7",
              "display":"Tobacco use"
            }
          ],
          "text":"Tobacco use"
        },
        "subject":{
          "reference":"Patient/id-1.1.466"
        },
        "encounter":{
          "reference":"Encounter/id-1.357620"
        },
        "valueCodeableConcept":{
          "coding":[
            {
              "system":"SNOMEDCT",
              "code":"228512004",
              "display":"Never chewed tobacco"
            }
          ],
          "text":"Never chewed tobacco"
        }
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Observation/id-fs.fs..1.581..0.0.0",
      "resource":{
        "resourceType":"Observation",
        "id":"id-fs.fs..1.581..0.0.0",
        "status":"final",
        "category":{
          "coding":[
            {
              "system":"urn:evident:observation-type",
              "code":"functional-status",
              "display":"Functional Status"
            }
          ]
        },
        "code":{
          "coding":[
            {
              "system":"LOINC",
              "code":"75275-8",
              "display":"Cognitive function finding"
            }
          ],
          "text":"Cognitive function finding"
        },
        "subject":{
          "reference":"Patient/id-1.1.462"
        },
        "effectiveDateTime":"2012-10-02T00:00:00-05:00",
        "issued":"2012-11-02T00:00:00.000-05:00",
        "valueCodeableConcept":{
          "coding":[
            {
              "system":"SNOMEDCT",
              "code":"386807006",
              "display":"Memory impairment"
            }
          ],
          "text":"Memory impairment"
        }
      }
    }
  ]
}';	
	 $decode_message = json_decode($json, true);
	 
	 foreach ($decode_message['entry'] as $k=>$v){
     $vitalVal = $this->vitalinsert_update($v['resource']);
	 //dump($vitalVal);
	}
	 return 'success';
	 
	 }
	 
public function vitalinsert_update($data){
$description= NULL ; $loinc_code = NULL ; $effective_date = NULL ; $readings = NULL ; $units= NULL ; $normal= 0 ; $severity= NULL ; $type= NULL ;$deleted= NULL ; $notes= NULL ; $recent_updation= NULL ; $api_patient_id = NULL ; $api_vital_id = NULL ; $effective = NULL; $low = NULL; $heigh = NULL ; $high_values = NULL; $low_values = NULL ;$highvalues = NULL; $Low = NULL; $High = NULL;$api_vital_id = NULL; $patient_id =NULL;

if (array_key_exists("category",$data))
     { 
 	if (isset($data['category']['coding']) == 1)
     {
	$type = $data['category']['coding'][0]['code'];
	}
		}
		

 	if (isset($data["id"]) == 1)
   {			
$api_vital_id = $data["id"];
}	
		
if (array_key_exists("category",$data)){
    if (isset($data['effectiveDateTime']) == 1){
       $effective_date= $data['effectiveDateTime'];
	   $effective = substr($effective_date,0,10);
     }
}
if (array_key_exists("category",$data)){
	if (isset($data['code']['coding']) == 1){
	$loinc_code= $data['code']['coding'][0]['code'];
	}
}
if (array_key_exists("category",$data)){
	if (isset($data['code']['text']) == 1){
	$description = $data['code']['text'];
		}

}

if (array_key_exists("category",$data)){
	if (isset($data['referenceRange']) == 1){
	$low = $data['referenceRange'][0]['text'];
//split the first values
	$first_values = head(explode(' ', trim($low)));
	
//split the last values	
	$last_valuse = strstr($low, ' ');
	
	//removing white space
	$white_space =preg_replace('/\s+/', '', $last_valuse);
	//removing "="
	$First_values = explode('=', $first_values);
	$Low = $First_values[1];
	
	$High_values = explode('=', $white_space);
	$High = $High_values[1];

//dump($high_values);

}
}

if (array_key_exists("category",$data)){
	if (isset($data['valueQuantity']['value']) == 1){
	$readings = $data['valueQuantity']['value'];
	}
	}


if($Low <= $readings and  $High >= $readings and $Low != NULL  and $High != NULL and $readings != NULL  ){
$normal = 1;
}



if (array_key_exists("category",$data)){
	if (isset($data['valueQuantity']['unit']) == 1){
	$units = $data['valueQuantity']['unit'];
	}
	}



if (array_key_exists("category",$data)){
	if (isset($data['subject']['reference']) == 1){
	$api_patient_id = $data['subject']['reference'];
$api = explode('/', $api_patient_id);
	$api_pat = $api[1];
//dump($api_pat);
	}

}
 $Patients = app('db')->select("SELECT id FROM admin_patient_rawdata where api_id='".$api_pat."' ");
 $val = json_decode( json_encode($Patients), true);
 $patient_id = $val[0]['id'];
 $vitalVal = $this->verifyData($description,$effective,$api_pat);
          $returnVal= $vitalVal;
         // dump($vitalVal);
	try{	 
 if(empty($returnVal)){
$actionVitals = DB::insert('insert into admin_vitals_raw_data (hospital_id, patient_id, description, loinc_code, effective_date, readings, units, normal, severity, type, deleted, notes, recent_updation, api_patient_id, api_vital_id,low,heigh,created_at) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[4,$patient_id,$description,$loinc_code,$effective,$readings,$units,$normal,0,$type,0,'NULL',1,$api_pat,$api_vital_id,$Low,$High,date('Y-m-d H:i:s')]);
	} 
	else {
 $array3 = json_decode(json_encode($returnVal),true);
 $id1 = $array3[0]['id'];
 $vital_raw  = admin_vitals_raw_data::find($id1);
 $vital_raw -> patient_id = $patient_id;
 $vital_raw -> description = $description;
 $vital_raw -> loinc_code = $loinc_code;
 $vital_raw -> effective_date = $effective ;
 $vital_raw -> readings = $readings;
 $vital_raw -> units = $units;
 $vital_raw -> normal = $normal;
 $vital_raw -> low = $Low;
 $vital_raw -> heigh = $High;
 $vital_raw -> severity ="0";
 $vital_raw -> type = $type;
 $vital_raw -> deleted = "0";
 $vital_raw -> notes = "NULL";
 $vital_raw -> recent_updation = "1";
 $vital_raw -> api_patient_id = $api_pat;
 $vital_raw -> api_vital_id =$api_vital_id;
 $vital_raw -> updated_at = date('Y-m-d H:i:s');
 $vital_raw->save();
 }
}
catch (\Exception $e) {
     return 'Failed';
}
}


public function verifyData($description,$effective,$api_pat)
    {
$val = app('db')->select("SELECT id FROM admin_vitals_raw_data where description='".$description."' and effective_date='".$effective."' and  api_patient_id='".$api_pat."' limit 0,1");
      return $val;
    }


}

?>