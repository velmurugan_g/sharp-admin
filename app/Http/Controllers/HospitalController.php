<?php

namespace App\Http\Controllers;

use App\Hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class HospitalController extends Controller{
	
	public function __construct() {
		$this->middleware('auth');
	}
 
    public function index(){ 
		$Hospitals  = Hospital::all();
        return response()->json($Hospitals);
    }
	
	 public function createHospital(Request $request){
		$hospital = new Hospital;
		if($request->get('name') != '')
		{
			$hospital->name =$request->get('name');
			$hospital->owner = $request->get('owner');
			$hospital->contactperson = $request->get('contactperson');
			$hospital->contactnumber = $request->get('contactnumber');
			$hospital->streetaddress = $request->get('streetaddress');
			$hospital->zip = $request->get('zip');
			$hospital->email = $request->get('email');
			$hospital->weburl = $request->get('weburl');
			$hospital->city = $request->get('city');
			$hospital->state = $request->get('state');
			$hospital->npi = $request->get('npi');
			$hospital->status = 'active';
			$hospital->medicareid = $request->get('medicareid');
			$hospital->medicareprovider = $request->get('medicareprovider');
			$hospital->description = $request->get('description');
			$hospital->save();
		}
    // return redirect()->back();
	return redirect('hospitals?i='.$hospital->id);
    }
	public function updateHospital(Request $request){
		 	$id = $request->get('id');
        	$hospital  = Hospital::find($id);
        	$hospital->name =$request->get('name');
			$hospital->owner = $request->get('owner');
			$hospital->contactperson = $request->get('contactperson');
			$hospital->contactnumber = $request->get('contactnumber');
			$hospital->streetaddress = $request->get('streetaddress');
			$hospital->zip = $request->get('zip');
			$hospital->email = $request->get('email');
			$hospital->weburl = $request->get('weburl');
			$hospital->city = $request->get('city');
			$hospital->state = $request->get('state');
			$hospital->npi = $request->get('npi');
			$hospital->medicareid = $request->get('medicareid');
			$hospital->medicareprovider = $request->get('medicareprovider');
			$hospital->description = $request->get('description');
			$hospital->save();
        echo "updated";
		return redirect('hospitals?i='.$request->get('id'));
    }
	public static function getHpl($hospital_id){ 
	static $hplname;
		$hpl_id = DB::select('select name from admin_hospital_entity where id='.$hospital_id);
		 foreach ($hpl_id as $hpl) {
		  $hplname = $hpl->name;
		 }
		return $hplname;
    }
	
	public static function Hplcount(){ 
		$hpl_id = DB::select('select distinct(count(*)) as cnt from admin_hospital_entity WHERE status = "active"');
		 foreach ($hpl_id as $hpl) {
		  $hplcnt = $hpl->cnt;
		 }
		return $hplcnt;
    }
	
	public static function getHplDetails($hospital_id){ 
		$hpl_det = DB::select('select * from admin_hospital_entity where id='.$hospital_id);
		return $hpl_det;
    }
	
	public static function getHplList(){ 
		$hpl_det = DB::select('select * from admin_hospital_entity WHERE status = "active"');
		return $hpl_det;
    }
	
	public static function getmoduleinsert(Request $request)
	
	{ 
		$hospitalid=$request->get('id');
		$action=$request->get('action');
		$type=$request->get('type');
		
		if($type == 'caregiver'){
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET caregiver_module ='.$action.'  where id ='.$hospitalid);
		}
		else if($type == 'treatment'){
		
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET treatments_module ='.$action.'  where id ='.$hospitalid);
		
		}else if($type == 'patients'){
		
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET feedback_module ='.$action.'  where id ='.$hospitalid);
		
		}else if ($type == 'vitals'){
		
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET vital_lab_module ='.$action.'  where id ='.$hospitalid);
		
		}else if($type == 'vendor'){
		
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET vendor_module ='.$action.'  where id ='.$hospitalid);
		}
		else if($type == 'vital_labs'){
		$hpl_det = DB::select('UPDATE admin_hospital_entity SET vital_labs_score ='.$action.'  where id ='.$hospitalid);
		}
		
		return $action;
    }
	

	
		public function check_email(Request $request)
    {
        $mail = $request->get('email');        
        $contactnumber = $request->get('contactnumber');      
        $emails = DB::select('select email from admin_hospital_entity where email="'.$mail.'"');       
        $phonenumbers = DB::select('select email from admin_hospital_entity where contactnumber="'.$contactnumber.'"');
        //echo count($users);die;
          if (count($emails)> 0)  
          {$mssg = "email";}   
           elseif (count($phonenumbers)> 0)  
          {$mssg = "phonenumber"; }
          else
         {$mssg = "sucess"; }
         return $mssg;
     }	
	 
	 
	 public function checkhospital_email_exist(Request $request)
    {
        $mail = $request->get('email');   
		$user_id = 	$request->get('user_id'); 	     
        $contactnumber = $request->get('contactnumber');      
        $emails = DB::select('select * from admin_hospital_entity where email="'.$mail.'"');       
        $phonenumbers = DB::select('select * from admin_hospital_entity where contactnumber="'.$contactnumber.'"');
       // echo count($emails);die;
		 $mssg = "sucess";
			foreach ($emails as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "email";
			  break;
			 }
		
			 foreach ($phonenumbers as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "phonenumber";
			  break;
			 }
		    return $mssg;
     }  
	
}