<?php


namespace App\Http\Controllers;

use App\admin_patient_rawdata;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

     
class PatientController extends Controller{

	public function __construct() {
	}
 
    public function patientlist(Request $request){
	
	  $hospitalid = $request->input('i');
//dump($name);
    	//$data = ['name' => 'vel','mobile' => '8870633623','city' => 'kovilpatti'];

    	// $json = '{"type": "donut", "name": "Cake", "toppings": [
     //    { "id": "5002", "type": "Glazed" },
     //    { "id": "5006", "type": "Chocolate with Sprinkles" },
     //    { "id": "5004", "type": "Maple" }]}';
     //   $yummy = json_decode($json, true);
     //    echo $yummy['toppings'][1]['type'];

//    $json = '{"resourceType":"Patient","id":"id-1.1.583",
//   "extension":[{
//       "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
//       "valueCodeableConcept":{
//         "coding":[
//           {
//             "system":"http://hl7.org/fhir/v3/AdministrativeGender",
//             "code":"F",
//             "display":"Female"
//           }
//         ]
//       }},{
//       "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
//       "valueCodeableConcept":{
//         "coding":[
//           {
//             "system":"http://hl7.org/fhir/v3/ethnicity",
//             "code":"2186-5",
//             "display":"Not Hispanic or Latino"
//           }
//         ],
//         "text":"Not Hispanic or Latino"
//       } } ],
//   "active":true,
//   "name":[{use":"official","family":["LOE" ], "given":["LAURA","L"] }],
//   "gender":"female",
//   "birthDate":"1953-03-06",
//   "address":[{
//       "line":[
//         "LAURA L LOE",
//         "220 HIGH AVE",
//         "APT. #17"
//       ],
//       "state":"MA",
//       "postalCode":"65432"}],
//   "maritalStatus":{"coding":[{"system":"MaritalStatusCodes","code":"D","display":"Divorced"}],"text":"Divorced}
// }';

  $json ='{
  "resourceType":"Bundle",
  "link":[ { "relation":"self",
      "url":"https://<base>/fhir/dstu2/Patient?_count=5"
    },
    {
      "relation":"next",
      "url":"https://<base>/fhir/dstu2/Patient?_count=5&page=2"
    }
  ],
  "entry":[
    {
      "fullUrl":"https://<base>/fhir/dstu2/Patient/id-1.1.583",
      "resource":{
        "resourceType":"Patient",
        "id":"id-1.1.583",
        "extension":[
          {
            "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/AdministrativeGender",
                  "code":"F",
                  "display":"Female"
                }
              ]
            }
          },
          {
            "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/ethnicity",
                  "code":"2186-5",
                  "display":"Not Hispanic or Latino"
                }
              ],
              "text":"Not Hispanic or Latino"
            }
          }
        ],
        "active":true,
        "name":[
          {
            "use":"official",
            "family":[
              "LOE"
            ],
            "given":[
              "LAURA",
              "L"
            ]
          }
        ],
        "gender":"female",
        "birthDate":"1953-03-06",
        "address":[
          {
            "line":[
              "LAURA L LOE",
              "220 HIGH AVE",
              "APT. #17"
            ],
            "state":"MA",
            "postalCode":"65432"
          }
        ],
        "maritalStatus":{
          "coding":[
            {
              "system":"MaritalStatusCodes",
              "code":"D",
              "display":"Divorced"
            }
          ],
          "text":"Divorced"
        }
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Patient/id-1.1.464",
      "resource":{
        "resourceType":"Patient",
        "id":"id-1.1.464",
        "extension":[
          {
            "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/AdministrativeGender",
                  "code":"M",
                  "display":"Male"
                }
              ]
            }
          },
          {
            "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/ethnicity",
                  "code":"2186-5",
                  "display":"Not Hispanic or Latino"
                }
              ],
              "text":"Not Hispanic or Latino"
            }
          }
        ],
        "active":true,
        "name":[
          {
            "use":"official",
            "family":[
              "HEPBEE"
            ],
            "given":[
              "HARRY",
              "G"
            ]
          }
        ],
        "telecom":[
          {
            "system":"phone",
            "value":"6179990000",
            "use":"home"
          },
          {
            "system":"email",
            "value":"hepbeeL@yahoo.com"
          }
        ],
        "gender":"male",
        "birthDate":"2000-08-01",
        "address":[
          {
            "line":[
              "HARRY G HEPBEE",
              "110 ELM STREET",
              "APT 3"
            ],
            "city":"CAMBRIDGE",
            "state":"MA",
            "postalCode":"2140"
          }
        ],
        "communication":[
          {
            "language":{
              "coding":[
                {
                  "system":"urn:ietf:bcp:47",
                  "code":"en",
                  "display":"English"
                }
              ],
              "text":"English"
            },
            "preferred":true
          }
        ]
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Patient/id-1.1.466",
      "resource":{
        "resourceType":"Patient",
        "id":"id-1.1.466",
        "extension":[
          {
            "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/AdministrativeGender",
                  "code":"M",
                  "display":"Male"
                }
              ]
            }
          },
          {
            "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/ethnicity",
                  "code":"2135-2",
                  "display":"Hispanic/Latino NOS"
                }
              ],
              "text":"Hispanic/Latino NOS"
            }
          }
        ],
        "active":true,
        "name":[
          {
            "use":"official",
            "family":[
              "SANCHEZ"
            ],
            "given":[
              "ABEL",
              "G"
            ]
          }
        ],
        "telecom":[
          {
            "system":"phone",
            "value":"2155551214",
            "use":"home"
          },
          {
            "system":"email",
            "value":"sangel@yahoo.com"
          }
        ],
        "gender":"male",
        "birthDate":"1980-03-20",
        "address":[
          {
            "line":[
              "ABEL G SANCHEZ",
              "6 MAPLE DRIVE",
              "APT 1"
            ],
            "city":"PHILADELPHIA",
            "state":"PA",
            "postalCode":"19136"
          }
        ],
        "communication":[
          {
            "language":{
              "coding":[
                {
                  "system":"urn:ietf:bcp:47",
                  "code":"en",
                  "display":"English"
                }
              ],
              "text":"English"
            },
            "preferred":true
          }
        ]
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Patient/id-1.1.462",
      "resource":{
        "resourceType":"Patient",
        "id":"id-1.1.462",
        "extension":[
          {
            "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/AdministrativeGender",
                  "code":"M",
                  "display":"Male"
                }
              ]
            }
          },
          {
            "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/ethnicity",
                  "code":"2186-5",
                  "display":"Not Hispanic or Latino"
                }
              ],
              "text":"Not Hispanic or Latino"
            }
          }
        ],
        "active":true,
        "name":[
          {
            "use":"official",
            "family":[
              "LERR"
            ],
            "given":[
              "TODD",
              "G"
            ]
          }
        ],
        "telecom":[
          {
            "system":"phone",
            "value":"5557259890",
            "use":"home"
          },
          {
            "system":"email",
            "value":"smithb@yahoo.com"
          }
        ],
        "gender":"male",
        "birthDate":"2009-06-07",
        "address":[
          {
            "line":[
              "TODD G LERR",
              "123 NORTH 102ND ST",
              "APT 4D"
            ],
            "city":"CAMP HILL",
            "state":"PA",
            "postalCode":"17012"
          }
        ],
        "communication":[
          {
            "language":{
              "coding":[
                {
                  "system":"urn:ietf:bcp:47",
                  "code":"en",
                  "display":"English"
                }
              ],
              "text":"English"
            },
            "preferred":true
          }
        ]
      }
    },
    {
      "fullUrl":"https://<base>/fhir/dstu2/Patient/id-1.1.468",
      "resource":{
        "resourceType":"Patient",
        "id":"id-1.1.468",
        "extension":[
          {
            "url":"http://hl7.org/fhir/ValueSet/v3-AdministrativeGender",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/AdministrativeGender",
                  "code":"F",
                  "display":"Female"
                }
              ]
            }
          },
          {
            "url":"http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept":{
              "coding":[
                {
                  "system":"http://hl7.org/fhir/v3/ethnicity",
                  "code":"2135-2",
                  "display":"Hispanic/Latino NOS"
                }
              ],
              "text":"Hispanic/Latino NOS"
            }
          }
        ],
        "active":true,
        "name":[
          {
            "use":"official",
            "family":[
              "DIODE"
            ],
            "given":[
              "MARIA"
            ]
          }
        ],
        "telecom":[
          {
            "system":"phone",
            "value":"7175559510",
            "use":"home"
          }
        ],
        "gender":"female",
        "birthDate":"1966-01-30",
        "address":[
          {
            "line":[
              "MARIA DIODE",
              "47 UPTOWN WAY"
            ],
            "city":"CARLISLE",
            "state":"PA",
            "postalCode":"17013"
          }
        ]
      }
    }
  ]
  }';
    $decode_message = json_decode($json, true);

    //var result=JSON.parse(json);
	//$arr = json_decode($json, true);
	//print_r($decode_message['entry']);
	foreach ($decode_message['entry'] as $k=>$v){

	  // print_r($v['resource']['name'][0]['family'][0]);
       $patientVal = $this->insert_update($v['resource'],$hospitalid);
      // foreach ($v as $parm=>$data){
        //   echo('<br>'.$parm.'<br>');
      // }
	   // echo $v; // etc.  ['resource']['name'][0]['family'][0]
	}
	
     //echo $json;
    // die;
     // $pat_id = $decode_message['id'];
     // $gender = $decode_message['gender'];
     // $dob = $decode_message['birthDate'];
     // $lastname =$decode_message['name'][0]['family'][0];
     // $firstname =$decode_message['name'][0]['given'][0];
     // $middlename =$decode_message['name'][0]['given'][1];
     // $maritalStatus = $decode_message['maritalStatus']['text'];
     // $active = 'active';
     // $careprovider = '';
     // $address = '';
     // $city = '';
     // $zip_code = '';
     // $phone = '';
     //$hospital_id = '';

    // $patientVal = $this->verifyData($firstname,$lastname,$dob);
     //$returnVal= $patientVal;
	 // if(empty($returnVal)){
	 // $actionPatient = Patient::create(['first_name' => $firstname, 'last_name' => $lastname, 'dob' => $dob, 'active' => $active, 'gender' => $gender, 'careprovider' => $careprovider, 'address' => $address, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'phone' => $phone,'hospital_id' => '2','bulkpush' => 0,'deleted' => 0,'update_rawdata'=> $decode_message]);
	 // }
	 // else {
	 // $array3 = json_decode(json_encode($returnVal),true);
	 // $id1 = $array3[0]['id'];
   // $actionPatient = $this->updatePatient(array('first_name' => $firstname, 'last_name' => $lastname, 'dob' => $dob, 'active' => $active, 'gender' => $gender, 'careprovider' => $careprovider, 'address' => $address, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'phone' => $phone,'hospital_id' => '2','bulkpush' => 1,'deleted' => '1','update_rawdata'=> $decode_message),$id1);
	//   }


      //echo $pat_id .'<br>'. $gender. '<br>'. $dob . '<br>'. $lastname . '<br>'. $firstname . '<br>'. $middlename . '<br>'. $maritalStatus  ;

    // return response()->json($data1);
	//return 'success';
	return redirect('apilist?i='.$hospitalid);
    }


    public function insert_update($data ,$hospital_id)
    {

     $phone = ''; $dob = '';$gender = '';
	 $email = ''; $pat_id = ''; $maritalStatus = '' ; $lastname = '';$firstname = '';$middlename = '';
	 $address = '';$address1 = '';$address2 = '';$city = '';$zip_code = ''; $state = '';
	 $race = ''; $race_code = '';
     $pat_id = $data['id'];
     $gender =  $data['gender'];
     $dob =  $data['birthDate']; 
      if (array_key_exists("name",$data))
      {  $lastname =$data['name'][0]['family'][0];
	     $firstname =$data['name'][0]['given'][0];
	      if (array_key_exists("['given'][1]",$data))
	      {
	         $middlename =$data['name'][0]['given'][1];
	      }
      }
     if (array_key_exists("maritalStatus",$data))
     {  
         $maritalStatus =$data['maritalStatus']['text'];
      }
	 if (array_key_exists("telecom",$data))
     {   
     	  $phone =$data['telecom'][0]['value'];
		 
     	 if (isset($data['telecom'][1]['system']) == 1)
     	 	{
     	 		$email = $data['telecom'][1]['value'];
     	 	}

      }
      if (array_key_exists("extension",$data))
     {   
     	 if (isset($data['extension'][1]['valueCodeableConcept']) == 1)
     	 	{
     	 		$race = $data['extension'][1]['valueCodeableConcept']['text'];
     	 		$race_code = $data['extension'][1]['valueCodeableConcept']['coding'][0]['code'];
     	 	}

      }
      
     $active = 'active';
     $careprovider = '';
     if (array_key_exists("address",$data))
     {
	     $address = $data['address'][0]['line'][0];
	     $address1 = $data['address'][0]['line'][1];
	    
	      if (isset($data['address'][0]['line'][2]) == 1)
	         {
		      $address2 = $data['address'][0]['line'][2];
		    }
		  if (isset($data['address'][0]['city']) == 1)
	       {
	           $city = $data['address'][0]['city'];
	       }
	      if (isset($data['address'][0]['state']) == 1)
	       {
	           $state = $data['address'][0]['state'];
	       }
	      if (isset($data['address'][0]['postalCode']) == 1)
	      	{ 
	      		$zip_code = $data['address'][0]['postalCode'];
	        }
      }
	     //$hospital_id =  $_GET["i"];

      // echo $hospital_id;die;

	     //  echo $pat_id .'<br>'.  $lastname . '<br>'. $firstname . '<br>'. $middlename . '<br>'. $maritalStatus .  '<br>'. $gender. '<br>'. $dob . '<br>'.$phone . '<br>'.$email . '<br>'.$address . '<br>'.$address1 . '<br>'.$address2  . '<br>'.$city. '<br>'.$state  . '<br>'.$zip_code . '<br>';
		   // echo('<br>');
		   // echo('<br>');
		  $patientVal = $this->verifyData($firstname,$lastname,$dob,$pat_id);
          $returnVal= $patientVal;
        //  dump($patientVal);
		  if(empty($returnVal)){
			 //$actionPatient = admin_patient_rawdata::create(['first_name' => $firstname, 'last_name' => $lastname, 'dob' => $dob, 'active' => $active, 'gender' => $gender, 'address' => $address, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'phone' => $phone,'hospital_id' => '2','bulkpush' => 0,'deleted' => 0,'update_rawdata'=> $data]);
		  	  $actionPatient = DB::insert('insert into admin_patient_rawdata (hospital_id, last_name, first_name, dob, gender, deleted, api_id, address, city, state, zip_code, country, temp_address, temp_city, temp_state, temp_zipcode, temp_country, phone, phone1, phone2, fax, email, maritalstatus, race,race_code, comments, api_type, bulkpush,recent_updation ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$hospital_id,$lastname,$firstname,$dob,$gender,0,$pat_id,$address1,$city,$state,$zip_code,'USA',$address1,$city,$state,$zip_code,'USA',$phone,$phone,$phone,NULL,$email,$maritalStatus,$race,$race_code,NULL,'Evident',1,1]);

			 }
			 else {
			 $array3 = json_decode(json_encode($returnVal),true);
			 $id1 = $array3[0]['id'];
			 $patient_raw  = admin_patient_rawdata::find($id1);
			 $patient_raw->first_name =$firstname;
			 $patient_raw->last_name = $lastname;
			 $patient_raw->dob = $dob;
			 $patient_raw->email = $email;
			 $patient_raw->gender = $gender;
			 $patient_raw->address = $address1;
			 $patient_raw->city = $city;
			 $patient_raw->state = $state;
			 $patient_raw->zip_code = $zip_code;
			 $patient_raw->phone = $phone;
			 $patient_raw->race_code = $race_code;
			 $patient_raw->race = $race;
			 $patient_raw->maritalStatus = $maritalStatus;
			 $patient_raw->recent_updation = "1";
			 //$patient_raw->status ='active';
			 $patient_raw->save();
		  // $actionPatient = $this->updateadmin_patient_rawdata(array('first_name' => $firstname, 'last_name' => $lastname, 'dob' => $dob, 'active' => $active, 'gender' => $gender, 'careprovider' => $careprovider, 'address' => $address, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'phone' => $phone,'hospital_id' => '2','bulkpush' => 1,'deleted' => '1','rawdata'=> $data),$id1);
			  }
			 // dump("sucess");
			  // return redirect('apilist?i='.$patient_raw->hospital_id);
			  return 'success';
    }

    public function verifyData($fname,$lname,$dob,$api_id)
    {
	 $val = app('db')->select("SELECT id FROM admin_patient_rawdata where first_name='".$fname."' and last_name='".$lname."' and dob='".$dob."'  AND api_id ='".$api_id."' limit 0,1");
      return $val;
    }


	
}