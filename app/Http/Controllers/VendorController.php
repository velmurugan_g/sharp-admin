<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests; 

class VendorController extends Controller{
	
	public function __construct() {
		$this->middleware('auth');
	}
 
    public function index(){ 
		$Vendors  = Vendor::all();
        return response()->json($Vendor);
    }
	
		public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
	
	 public function createVendor(Request $request){
	 $user = new User;
	 
		$vendor = new Vendor;
//print_r($request->get('weekdayadd'));
//print_r($request->get('availabletime'));
		
		if($request->get('vendorname') != '')
		{
	//	$geturl = DB::insert('insert into admin_user_entity (firstname,username, hospital_id, email, password, city, zipcode) values (?,?,?,?,?,?,?)', [$request->get('vendorname'),$request->get('username'),$request->get('hospital_id'),$request->get('email'),hash("sha1",$request->get('password')),$request->get('city'),$request->get('zip')]);
		
		
			$user->firstname =$request->get('vendorname');
			$user->city = $request->get('city');
			$user->zipcode = $request->get('zip');
			$user->email = $request->get('email');
			$user->hospital_id = $request->get('hospital_id');
			$user->username = $request->get('username');
			$user->password = hash("sha1",$request->get('password'));
			$user->email = $request->get('email');
			$user->status ='active';
			$user->save();
		
		
		
			$vendor->name =$request->get('vendorname');
			$vendor->hospital_id = $request->get('hospital_id');
			$vendor->user_id = $user->id;
			$vendor->email = $request->get('email');
			$vendor->password = hash("sha1",$request->get('password'));
			$vendor->state = $request->get('state');
			$vendor->address1 = $request->get('streetaddress1');
			$vendor->address2 = $request->get('streetaddress2');
			$vendor->city = $request->get('city');
			$vendor->zip = $request->get('zip');
			$vendor->country = $request->get('country');
			$vendor->phonenumber = $request->get('phonenumber');
			$vendor->username = $request->get('username');
			$vendor->isdefault = $request->get('defaultvendor');
			$vendor->coveragearea = $request->get('zipcodes');
			$vendor->quality_level = $request->get('qualitylevel');
			$vendor->vendor_type = $request->get('vendor_type');
			$vendor->patient_manage = $request->get('managepatients');
			$vendor->service_id = "0";
			$vendor->save();
			
		$remember_token = $this->generateRandomString();
		$cal = date("Y-m-d H:i:s");
		
		$vendor_type = DB::insert('insert into pat_caregiver_contact(Hospital_ID, Caregiver_Name, Availability_ID, alerts, User_Id, Status, type, Record_created, Record_Updated) values (?,?,?,?,?,?,?,?,?)',[$request->get('hospital_id'),$request->get('vendorname'),0,0,$user->id,'active','vendor',$cal,$cal]);
		
		$val = DB::select('Select max(id) as adminid from admin_user_entity');
		foreach ($val as $val2) {
		  $val23 = $val2->adminid;
		 }
		 
		 $valvend = DB::select('Select max(id) as adminid from admin_vendors');
		foreach ($valvend as $val2) {
		  $valvends = $val2->adminid;
		 }
		 
		
		$getroles = DB::insert('insert into admin_user_roles_entity (user_id, role_id,record_created_date,record_updated_date) values (?,?,?,?)', [$val23, '9',$cal,$cal]);
		
		//$getins_services = DB::insert('insert into admin_user_roles_entity (user_id, role_id,record_created_date,record_updated_date) values (?,?,?,?)', [$val23, '9',$cal,$cal]);
//		
//		$getins_zip = DB::insert('insert into admin_user_roles_entity (user_id, role_id,record_created_date,record_updated_date) values (?,?,?,?)', [$val23, '9',$cal,$cal]);





		$vendservices = $request->get('vendservices');
	    $vendsize = sizeof($vendservices);
			for($l=0;$l<$vendsize;$l++)
		{
			
				$geturl = DB::insert('insert into admin_vendor_services(hospital_id,vendor_id,service_id, services) values (?,?,?,?)', [$request->get('hospital_id'),$valvends,$vendservices[$l],$vendservices[$l]]);
		}
		
		$str = $request->get('zipcodes');
		$vendzipcode = explode("~",$str);
		$vendsize = sizeof($vendzipcode);
		 for($j=0;$j<$vendsize;$j++)
		 {
		 $geturl = DB::insert('insert into admin_vendor_zipcode(hospital_id, vendor_id, zip_code, created_at, updated_at) values(?,?,?,?,?)',[$request->get('hospital_id'),$valvends,$vendzipcode[$j],$cal,$cal]);
		 }
		
		
		$availtime = $request->get('availabletime');
		$weekday = $request->get('weekdayadd');
		$avsize = sizeof($availtime);
		$weekdaysize = sizeof($weekday);
		for($k=0;$k<$weekdaysize;$k++)
		{
			for($n=0;$n<$avsize;$n++)
			{
			$getval = DB::select('select Timing from caregiver_timing where id='.$availtime[$n]);
				foreach ($getval as $val2) {
				$val22 = $val2->Timing;
				}
				$geturl = DB::insert('insert into admin_vendors_availability (vendor_id,vendor_timing_id, available_day, availability_date, availability_timing,deleted,record_created,record_updated) values (?,?,?,?,?,?,?,?)', [$valvends,$availtime[$n],$weekday[$k],'2018-10-07',$val22,'0','2018-10-07','2018-10-07']);
			}
		}
		}
	return redirect('hospitals?i='.$request->get('hospital_id'));
    }
	
	 public function updateVendor(Request $request){
	   $id = $request->get('vendor_id');
	   $userid=$request->get('user_id');
		$vendor  = Vendor::find($id);
		$user  = User::find($userid);
		if($request->get('vendorname') != '')
		{
		if($request->get('defaultvendor'))
		{
		$vendd = "1";
		}
		else
		{
		$vendd = "0";
		}
			$user->firstname =$request->get('vendorname');
			$user->city = $request->get('city');
			$user->zipcode = $request->get('zip');
			$user->email = $request->get('email');
			$user->hospital_id = $request->get('hospital_id');
			$user->username = $request->get('username');
			$user->password = hash("sha1",$request->get('password'));
			$user->email = $request->get('email');
			$user->status ='active';
			$user->save();
		
		
			$vendor->name =$request->get('vendorname');
			$vendor->user_id = $request->get('user_id');
			$vendor->email = $request->get('email');
			$vendor->state = $request->get('state');
			$vendor->address1 = $request->get('streetaddress1');
			$vendor->address2 = $request->get('streetaddress2');
			$vendor->city = $request->get('city');
			$vendor->zip = $request->get('zip');
			$vendor->country = $request->get('country');
			$vendor->phonenumber = $request->get('phonenumber');
			$vendor->username = $request->get('username');
			$vendor->isdefault = $vendd;
			$vendor->coveragearea = $request->get('zipcodes');
			$vendor->quality_level = $request->get('qualitylevel');
			$vendor->vendor_type = $request->get('vendor_type');
			$vendor->patient_manage = $request->get('managepatients');
			$vendor->service_id = '0';
			$vendor->save();
		}	
		$remember_token = $this->generateRandomString();
		
		$valupdate = DB::select('update admin_vendors_availability set deleted=1 where vendor_id='.$request->get('vendor_id'));
		
		$valupdate = DB::select('delete from admin_vendor_services where vendor_id='.$request->get('vendor_id'));
		
		$valupdate =DB::select('delete from admin_vendor_zipcode where vendor_id='.$request->get('vendor_id'));
		
		$vendservices = $request->get('vendservices');
	    $vendsize = sizeof($vendservices);
			for($l=0;$l<$vendsize;$l++)
		{
			
				$geturl = DB::insert('insert into admin_vendor_services(hospital_id,vendor_id,service_id, services) values (?,?,?,?)', [$request->get('hospital_id'),$request->get('vendor_id'),$vendservices[$l],$vendservices[$l]]);
		}
		
		
		$availtime = $request->get('availabletime');
		$weekday = $request->get('weekdayadd');
		$avsize = sizeof($availtime);
		$weekdaysize = sizeof($weekday);
		for($k=0;$k<$weekdaysize;$k++)
		{
			for($n=0;$n<$avsize;$n++)
			{
			$getval = DB::select('select Timing from caregiver_timing where id='.$availtime[$n]);
				foreach ($getval as $val2) {
				$val22 = $val2->Timing;
				}
				$geturl = DB::insert('insert into admin_vendors_availability (vendor_id,vendor_timing_id, available_day, availability_date, availability_timing,deleted,record_created,record_updated) values (?,?,?,?,?,?,?,?)', [$request->get('vendor_id'),$availtime[$n],$weekday[$k],'2018-10-07',$val22,'0','2018-10-07','2018-10-07']);
			}
		}
		
		
		$str = $request->get('zipcodes');
		$vendzipcode = explode("~",$str);
		$vendsize = sizeof($vendzipcode);
		 for($j=0;$j<$vendsize;$j++)
		 {
		 $geturl = DB::insert('insert into admin_vendor_zipcode(hospital_id, vendor_id, zip_code, created_at, updated_at) values(?,?,?,?,?)',[$request->get('hospital_id'),$request->get('vendor_id'),$vendzipcode[$j],'2018-10-07','2018-10-07']);
		 }
		
		
	return redirect('hospitals?i='.$request->get('hospital_id'));
    }
	
	public static function getVendorlist($hospital_id){ 
		$vendor_details = DB::select('select * from admin_vendors where hospital_id='.$hospital_id);
		return $vendor_details;
    }

    public static function deletevendor(Request $request){ 

    	$vendor_id=$request->get('vendor_id');
		$hospital_id=$request->get('hospital_id');
		
		$hpl_det = DB::select('UPDATE admin_vendors SET deleted = 1,updated_at = now()  where id ='.$vendor_id.' AND  hospital_id = '.$hospital_id);
       return 'sucess';
    }
	
	public static function getTasklookup(){ 
		$task_lookup = DB::select('select * from pat_caregiver_task_lookup where Deleted=0');
		return $task_lookup;
    }
	
	public static function getCaregiverAvailable(Request $request){ 
	$patientid = $request->get("patientid");$emailid = $request->get("emailid");$phone = $request->get("phone");
		$task_lookup = DB::select('SELECT *,pat_caregiver_contact.ID as pat_caregiver_ID FROM  admin_user_entity
					   JOIN pat_caregiver_contact on pat_caregiver_contact.user_id = admin_user_entity.ID
					   LEFT OUTER JOIN caregiver_patients on caregiver_patients.caregiver_contact_id = pat_caregiver_contact.ID
					   WHERE admin_user_entity.email =  '.$emailid.'  AND caregiver_patients.Patient_ID = '.$patientid);
		return $task_lookup;
    }
	
	public static function getEmergencyOrder(Request $request){ 
	$patientid = $request->get("patientid");$emergencyorder = $request->get("emergencyorder");$caregiver_id = $request->get("caregiver_id");
		$getEmergency = DB::select('SELECT * FROM  caregiver_patients  WHERE Patient_ID = '.$patientid .' AND  emer_order = '.$emergencyorder.'  AND caregiver_contact_id != '.$caregiver_id.'');
		return $getEmergency;
    }
	
	public static function getAdminVendorServices(){ 
		$getServices = DB::select('SELECT * FROM  admin_vendor_services_lookup');
		return $getServices;
    }
	
	 public static function getUserID($userid){ 
		$getval_id = DB::select('select * from admin_vendors where id='.$userid);
		return $getval_id;
    }
	
	 public static function getAvailRecords($vendor_id,$day){ 
	  static $valg;
		$getval_id = DB::select('SELECT distinct(available_day) as available_day from admin_vendors_availability where vendor_id='.$vendor_id.' and available_day="'.$day.'"');
		foreach ($getval_id as $val2) {
		  $valg = $val2->available_day;
		 }
		 return $valg;
    }
	
	 public static function gettimeRecords($vendor_id,$idd){ 
	  static $valgs;
		$getval_id = DB::select('SELECT distinct(vendor_timing_id) from admin_vendors_availability where vendor_id='.$vendor_id.' and vendor_timing_id='.$idd);
		foreach ($getval_id as $val2) {
		  $valgs = $val2->vendor_timing_id;
		 }
		 return $valgs;
    }
	
	 public static function getserviceRecords($vendor_id,$idd,$service_id){ 
	  static $valgs;
		$getval_id = DB::select('SELECT distinct(service_id) from admin_vendor_services where vendor_id='.$vendor_id.' and hospital_id='.$idd.' and service_id='.$service_id);
		foreach ($getval_id as $val2) {
		  $valgs = $val2->service_id;
		 }
		 return $valgs;
    }
	

	
	public static function getvendor_type_All(){ 
		$getvendoretype = DB::select('select id,description from admin_vendor_type_lookup');
		/* foreach ($getrole_id as $getrole) {
		  $role_id = $getrole->role_id;
		 }*/
		return $getvendoretype;
    }
	
	public static function insertavailableVendor(Request $request){ 
	$day = $request->get("day");$caregiverid = $request->get("caregiverid");$Patient_ID = $request->get("Patient_ID");$avaliablity_time = $request->get("avaliablity_time");$value_checked = $request->get("value_checked");$caregiver_timing_id = $request->get("caregiver_timing_id");
		$getEmergency = DB::select('SELECT * FROM caregiver_timing WHERE ID in ('.$caregiver_timing_id.')');
		return $getEmergency;
    }
	
	
	public function username_exists(Request $request)
    {
        $mail = $request->get('email');
        $username = $request->get('username');
        $contactnumber = $request->get('contactnumber');      
        $emails = DB::select('select email from admin_user_entity where email="'.$mail.'"');
        $usernames = DB::select('select email from admin_user_entity where username="'.$username.'"');
        $phonenumbers = DB::select('select email from admin_user_entity where contactnumber="'.$contactnumber.'"');
        //echo count($users);die;
          if (count($emails)> 0)  
          {$mssg = "email";}
           elseif (count($usernames)> 0)  
          {$mssg = "username"; }
           elseif (count($phonenumbers)> 0)  
          {$mssg = "phonenumber"; }
          else
         {$mssg = "sucess"; }
         return $mssg;
     }	
	 
	 public function check_email_exist(Request $request)
    {
        $mail = $request->get('email');
        $username = $request->get('username');
        $contactnumber = $request->get('contactnumber'); 
        $user_id = 	$request->get('user_id'); 	
        $emails = DB::select('select id,email from admin_user_entity where email="'.$mail.'"');
        $usernames = DB::select('select * from admin_user_entity where username="'.$username.'"');
        $phonenumbers = DB::select('select * from admin_user_entity where contactnumber="'.$contactnumber.'"');
        //echo count($users);die;
		 $mssg = "sucess";
			foreach ($emails as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "email";
			  break;
			 }
			 foreach ($usernames as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "username";
			  break;
			 }
			 foreach ($phonenumbers as $user)
			 { 
			 if($user->id != $user_id )
			  $mssg = "phonenumber";
			  break;
			 }
		    return $mssg;
     }	
	 
	 
	
}