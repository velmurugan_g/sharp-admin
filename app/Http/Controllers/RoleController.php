<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

class RoleController extends Controller{
	
	protected $rolenames;
	
	public function __construct() {
		$this->middleware('auth');
	}
 
    public function index(){ 
		$Roles  = Role::all();
        return response()->json($Roles);
    }
	
	 public function createRole(Request $request){
		$role = new Role;
		if($request->get('rolename') != '')
		{
			$role->rolename =$request->get('rolename');
			$role->description = $request->get('description');
			$role->accesslevel = $request->get('accesslevel');
			$role->save();
		}
     //return redirect()->back();
	  return redirect('roleslist');
    }
	
	public static function getRolesAll(){ 
		$getRoles = DB::select('select id,rolename from admin_roles_entity where rolename!="caregiver"');
		/* foreach ($getrole_id as $getrole) {
		  $role_id = $getrole->role_id;
		 }*/
		return $getRoles;
    }
	
	public static function getRoleName($userid){ 
	static $rolenames;
		$roleidval = DB::select('select rolename from admin_roles_entity where id=(select role_id from admin_user_roles_entity where user_id='.$userid.' limit 0,1)');
		 foreach ($roleidval as $roleidval2) {
		  $rolenames = $roleidval2->rolename;
		 }
		return $rolenames;
    }
	
	public static function getRolesAdmin(){ 
		$getRoles = DB::select('select id,rolename from admin_roles_entity where rolename !="Super-admin" and rolename!="caregiver"');
		/* foreach ($getrole_id as $getrole) {
		  $role_id = $getrole->role_id;
		 }*/
		return $getRoles;
    }
}