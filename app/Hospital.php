<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_hospital_entity';
    protected $fillable = ['name', 'owner', 'contactperson', 'email', 'contactnumber', 'streetaddress', 'city', 'state', 'zip', 'weburl'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['medicareprovider', 'description', 'createdby', 'record_created_date', 'record_updated_date', 'npi', 'medicareid'];
}
