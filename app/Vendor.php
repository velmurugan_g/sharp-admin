<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_vendors';
    protected $fillable = ['hospital_id', 'user_id', 'email', 'password', 'status', 'address1', 'address2', 'city', 'zip','country', 'phonenumber', 'username'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at'];
}
