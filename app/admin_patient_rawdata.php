<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin_patient_rawdata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_patient_rawdata';
    protected $fillable = [ 'hospital_id', 'last_name', 'first_name', 'dob', 'gender', 'deleted', 'api_id', 'address', 'city', 'state', 'zip_code', 'country', 'temp_address', 'temp_city', 'temp_state', 'temp_zipcode', 'temp_country', 'phone', 'phone1', 'phone2', 'fax', 'email', 'maritalstatus', 'race', 'comments', 'api_type', 'bulkpush','rawdata'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at'];
    //protected $hidden = ['created_at','updated_at'];
}

