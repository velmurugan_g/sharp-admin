<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin_vitals_raw_data extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_vitals_raw_data';
    protected $fillable = [ 'hospital_id', 'patient_id', 'description', 'loinc_code', 'effective_date', 'readings', 'units', 'normal', 'severity', 'type', 'deleted', 'notes', 'recent_updation', 'api_patient_id', 'api_vital_id','low','heigh'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at'];
    //protected $hidden = ['created_at','updated_at'];
}

