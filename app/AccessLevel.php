<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'admin_accesslevel_entity';
    protected $fillable = ['accesslevelname', 'description', 'accesslevels', 'accesslevel'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id','created_at','updated_at'];
}
