<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
 Route::get('/inserthospital', function () {
     return view('sharpviews/inserthospital');
 });
  Route::get('/hospitals', function () {
     return view('sharpviews/hospitals');
 }); 
  
Route::get('/insertuser', function () {
   //  return view('sharpviews/insertuser');
	 $gethospitallist = DB::table('admin_hospital_entity')->get();
	 $getrolelist = DB::table('admin_roles_entity')->get();
    return view('sharpviews/insertuser', ['gethospitallist' => $gethospitallist,'getrolelist' =>  $getrolelist]);
 });
 Route::get('/editusers', function () {
   //  return view('sharpviews/insertuser');
    return view('sharpviews/editusers');
 });
  Route::get('/edithospitals', function () {
     return view('sharpviews/edithospitals');
 });
Route::get('/insertroles', function () {
     return view('sharpviews/insertroles');
 });
 Route::get('/insertvendor', function () {
     return view('sharpviews/insertvendor');
 });
  Route::get('/editvendor', function () {
     return view('sharpviews/editvendor');
 });
 Route::get('/insertaccesslevel', function () {
     return view('sharpviews/insertaccesslevel');
 });
  Route::post('/createhospital','HospitalController@createHospital');
   Route::post('/updatehospital','HospitalController@updateHospital');
     Route::post('/updateuser','UserController@updateUser');
  Route::post('/createvendor','VendorController@createVendor');
  Route::post('/updatevendor','VendorController@updateVendor');
  Route::post('/createuser','UserController@createUser');
   Route::post('/createrole','RoleController@createRole');
  Route::get('/hospitalslist', function () {
   // $hospitalslist = DB::table('admin_hospital_entity')->get();
   	$hospitalslist = DB::select('select * from admin_hospital_entity where deleted  <> 1 AND status = "active"');
    return view('sharpviews/hospitalslist', ['hospitalslist' => $hospitalslist]);
});
Route::get('/userslist', function () {
    //$userslist = DB::table('admin_user_entity')->get();
	$userslist = DB::select('select * from admin_user_entity where deleted  <> 1 ');
    return view('sharpviews/userslist', ['userslist' => $userslist]);
});
Route::get('/roleslist', function () {
    $roleslist = DB::table('admin_roles_entity')->get();
    return view('sharpviews/roleslist', ['roleslist' => $roleslist]);
});
Route::get('/insertapi', function () {
     return view('sharpviews/insertapi');
 });
  Route::get('/apilist', function () {
    return view('sharpviews/apilist');
});
Route::get('/switch', function () {
    return view('sharpviews/switch');
});
Route::get('/editapi', function () {
     return view('sharpviews/editapi');
 });
 Route::post('/createapi','ApiController@createApi');
  Route::post('/updateapi','ApiController@updateapi');
  
  
  
  
  Route::get('sendemail', function () {

    $data = array(
        'name' => "Learning Laravel",
    );

    Mail::send('sharpviews.welcome1', $data, function ($message) {

        $message->from('sharpkatenterprise@gmail.com', 'Learning Laravel');

        $message->to('kalyankumar@maniindiatech.com')->subject('Learning Laravel test email');

    });

    return "Your email has been sent successfully";

});
Route::get('/getmsg','VendorController@username_exists');
Route::get('/checkemail','VendorController@check_email_exist');

Route::get('/getmessage','UserController@username_exists');
Route::get('/checkuseremail','UserController@checkuser_email_exist');

Route::get('/getemail','HospitalController@check_email');
Route::get('/checkhospitalemail','HospitalController@checkhospital_email_exist');

Route::get('/module_authorization','HospitalController@getmoduleinsert');


Route::get('/deletvendor','VendorController@deletevendor');


//Route::get('gethospitallist', 'HospitalController@index');
/*Route::get('/gethospitallist', function () {
    $gethospitallist = DB::table('admin_hospital_entity')->get();
    return view('sharpviews/insertuser', ['gethospitallist' => $gethospitallist]);
});*/